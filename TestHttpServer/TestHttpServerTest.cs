﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 08/12/2018
 * Time: 10:14
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NUnit.Framework;

namespace TestHttpServer
{
	/// <summary>
	/// Description of TestHttpServerTest.
	/// </summary>
	[TestFixture]
	public class TestHttpServerTest
	{
		[Test, Explicit]
		public void TestServerRun()
		{
			var serv = new TestHttpServer("localhost", 8090);
			serv.Listen(x => x.LocalPath == "/verify");
		}
	}
}
