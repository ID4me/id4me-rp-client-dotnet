﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 08/12/2018
 * Time: 10:09
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Net;
using System.Text;
using System.Threading;

namespace TestHttpServer
{
    public class TestHttpServer
    {
        // To enable this so that it can be run in a non-administrator account:
        // Open an Administrator command prompt.
        // netsh http add urlacl http://+:8008/ user=Everyone listen=true

        private HttpListener listener = null;
        private int requestNumber = 0;
        private DateTime startupDate = DateTime.UtcNow;
        private string prefix;
        private Semaphore waitSem;
        private bool stop_listening = false;

        public TestHttpServer(string host, int port=8080)
        {
        	prefix = string.Format("http://{0}:{1}/", host, port);
        	waitSem = new Semaphore(0, 1);
            if (!HttpListener.IsSupported)
            {
            	throw new InvalidOperationException("HttpListener is not supported on this platform.");
            }
        }
        
        public void Listen(Func<Uri, bool> handleRequest)
        {
            using (listener = new HttpListener())
            {
                listener.Prefixes.Add(prefix);
                listener.Start();
                // Begin waiting for requests.
                while (stop_listening == false)
                {
	                listener.BeginGetContext(GetContextCallback, handleRequest);
	                waitSem.WaitOne();
                }
                listener.Stop();
            }
        }

        private void GetContextCallback(IAsyncResult ar)
        {
        	try
        	{
	            int req = ++requestNumber;
	
	            // Get the context
	            var context = listener.EndGetContext(ar);
		
	            // get the request
	            var NowTime = DateTime.UtcNow;
	
	            Console.WriteLine("{0}: {1}", NowTime.ToString("R"), context.Request.RawUrl);
	
	            var responseString = string.Format("<html><body>Your request, \"{0}\", was received at {1}.<br/>It is request #{2:N0} since {3}.",
	                context.Request.RawUrl, NowTime.ToString("R"), req, startupDate.ToString("R"));
	
	            byte[] buffer = Encoding.UTF8.GetBytes(responseString);
	            // and send it
	            var response = context.Response;
	            response.ContentType = "text/html";
	            response.ContentLength64 = buffer.Length;
	            response.StatusCode = 200;
	            response.OutputStream.Write(buffer, 0, buffer.Length);
	            response.OutputStream.Close();
	            
	            var f = ar.AsyncState as Func<Uri, bool>;
	            if (f != null)
	            	stop_listening = f(context.Request.Url);
        	}
        	finally
        	{
        		waitSem.Release();
        	}
        }
    }
}