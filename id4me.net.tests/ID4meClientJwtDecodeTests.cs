﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 00:40
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using ID4me.Jose.jwk;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using org.id4me.Exceptions;

namespace org.id4me
{
	[TestFixture]
	public class ID4meClientJwtDecodeTests
	{
		const string jwks = "{\"keys\":[{\"d\":\"LiY9nb5v4NW8-iBtU3Z_Bx-PNFzX57qeaEqh2AZuuvFLZpBNvKhELJ0_Re308LweyCYVJHaP6MUrVs9i2PwGTtNxxuZ835K09T_9wc5OcNIjzI5WxLc8j1Wwd65v1QlndmXh4MS9rPjPC3us4modamFcjf5I0Lg9o-0miCt77SYROl_ZuHCX8QVCVrySU4UkU9xpZONpkDn25CpwnrftRhO1hP60HIiXWA6vQvYYHpRX9f5wuqIshcoMzXccSHf_TMY9UEAsxpvIqO98eRCxWNF75RTBynddqoSWVBnP_JwCUWOG3ZtwS7EGYd5HncNKdramrh0XK-JOzS6xb7UyzQ\",\"dp\":\"I6knDqeSJTdbYw-OfXM2KmqgUZ-z6QaVqpLDPyNeTfJJTwklH1i9T5i_6t849fOKBOjrYhZ-Poz3t1tVJ7n77_colyr-i7o3irMoVnMI-m8EalwVGV61jJ7BlAruhnvrINNrGauUJ9E_2kkcFaJtlQy3LPN2r8e0oX2qH9NANkk\",\"dq\":\"DfKB5JouG30jiVnFJljMwMKTJw47o4bSJ5BGqZIxfNjVgtQir2-_Ekzh4F_kVMxFIDhSc-gKBFtDw1SMkbfZUZl8gdhZe9ejvhXuh08NuTKDbdayuWNCv4gGd7kwTIvjjI545uaLsc5fql7-D9Ai6gF40lfzZZp9If0yUve8CAM\",\"e\":\"AQAB\",\"kid\":\"9664a935-c49d-4914-963a-110aba5d6202\",\"kty\":\"RSA\",\"n\":\"saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ\",\"p\":\"5FJM0z-LeWKNtz7DZsheYKhViieNxgor2JhnsCJZd8L1IPlmwJ8tueyUjG7wbFrVsdfKp4Mo15CDXAMdMBjmvSa6oY1_hJIjiWvRbAbJ0MgMo60nc6Pv4H7N807Lg9AFhVDUaQIL47ldOYvp9cJBnNGWZoGKop-SllDsB4mN_m8\",\"q\":\"xy-TgNAeqeF8mLPE37KE99-jtcVaMR3wZsfytumwZW-lm8oVgmUcnt_u4157_ni3OidPgdcpItNvGi5L4h9MOmWJI7Hc3aSZnj7ZTFjeQ4OFg7eCmUxENtMXseGK1eRyfzE6e-y7PGtp3eVbz-yD_yED81LwTKSbx6zJD3S0D8s\",\"qi\":\"tKkmN7wcmG63hAQAoq9oD0KYXjv6V5bnrr5NBHHWtO_fqHpofqhHizYemmuQnvad8I7kg6h1iN_abhxMaE5gtUi-kzdRZy1hx_KU3w5XO2F6h60WKfQEuXtIkPFaGjtLPhzGTDDaisOsOYltKQOIuUkjaO3ZBatzGSi9BLLeqT8\"}]}";

		const string reg_str = @"{
	            ""client_id"": ""lbgdkmy7lgo4s"",
	            ""client_secret"": ""HF2Lfedqhgu0UprrqTxqNrKPeydqei4nd51toWbG0JY"",
	            ""userinfo_signed_response_alg"": ""RS256"",
	            ""id_token_signed_response_alg"": ""RS256"",
	            ""client_secret"": ""HF2Lfedqhgu0UprrqTxqNrKPeydqei4nd51toWbG0JY"",
	            ""private_jwks"": " + jwks + "}";

		IEnumerable<JWK> auth_jwks;

		ID4meContext ctx;

		ID4meClient client;

		public ID4meClientJwtDecodeTests()
		{
			auth_jwks = JWKS.Parse("{   \"keys\":[      {         \"kid\":\"0d581143-aa56-4130-a371-86776e1cd338\",         \"kty\":\"RSA\",         \"d\":\"M9gzMLK830330N8jdeyHCoMvfPEslN8Nrz8jSpIduh8ACH24R5rBJ2nQjfivq7LNlEJ7n2oZMgPU28u9e8ImNbrkN5ZQSzfXZQPjbJe2zZLfijJr9hRnJuwflU0H0HXKm1T9z0FK-lBQ-XYJg6fKQKzJNBWn_hNUojIM3ZcTj5gaAaN6avad8VG7s4RS5kvC7dFUb0yVVs86q8GCDobGWfOaxf74iNcIdKyvjTKAKCw95E3p614GuKbAV9NCmmt0YQkfRey-_hCLPcK711KNMyze326e3lbFI-mVkVgub8Fs0ztqo4uD2SnsGBTI0_xzHqYY1mMcmGaeNHuJLtoa8Q\",         \"dp\":\"bsfO1sfNUc06WSCiBIUoOtHN9MKmzdXN7c5yIpOtpIjTuUSbjcaY8fvyTyirONbaqWvCP3kDhxsump65t36wMYDOEcxHjjUbI6KB9E0quJV4utwMNqpoxLnXI1nSNOjjNiaUPa5nnEJpa8YkCNQOx0pnzDBc4fF9XKi2nH7pwSU\",         \"dq\":\"N8-P3PBdsgdDUalDoNv0muWSTwkhTr43MhckHDGZTHTfLz5d4_GG7xSdZ5ZzZMfZ67OJnLyJgSYVr77ok1rUvfLWrqGVvEIACgdAWPst8NGJFNnoveckY1RirTBVs-sOTqmB86KhR3gb7tTugFFZ5O8ErQMB6TanvA5q2IOC55s\",         \"e\":\"AQAB\",         \"n\":\"zD1qUPjiTRu_OmXgMX0XUFx72NzA4LmvF8Wvwsay0NyEVOiu5KfOTF8pLQO37VNsoIgnbFw3Vo9HVaE6SIELwTuNTYjUwGrli5d9RK4f90kE8NOXfYNAIMv26XsfqUvRuq9SPSuNcX5L3Ggxo1IkM1IUU3LcDHwiD_vMyDta5MuWUlWAxRcPTAo50lZCbB7HSDThn1rBjfNbeA0KVEzrv5tLirqdSgNOh4h7igPN8YU-ZMmaLKedKoUvjghJxqB89JzrftR6oqUaR71zds1RK1Q8l5w996-gIQaKMVX2vuj6FflxGn5lWJIHDVF6xVX99VtP1gzSMpCmCvnUVHBRAQ\",         \"p\":\"9aBT9tefoo2f_Ohq4s40wyGYBYxa-oyDVMJZ1xEZPWSOQ_BzuxPba9YeRVkSh7extR8FRsnBrUtQIRim5e8gM_lp-v87c_DoMFeguCiYEQvgeiA-zEhDEBR5KNYCCHH6nzQbi0YCRmEEXeF6KYnlY1EKiW7i-J_FXeKdo7jlV5c\",         \"q\":\"1N2f2lcTcsbqEX_c3w2nZZC6AFHEjaW26awYTeofG9kFM4bsHX2Ar9rC2Esh8S4Af_g2eQmGEQIGE3dbMoMEtGJW0fuQ01DMd3mXgXmweNkwJIQVZDC45ZEYl0s8TDtUN3qjZLUPPho9_dZ0BJYmUrMxVEaaQwWKnGBYx2BW7yc\",         \"qi\":\"CgzzpEX_q_ke_S-3YtllJpO7iVfmi2YxJ68eaS8PtnGGk0lGeye0X7B-QxOKFm8bn8i4qBAe85gE9Wy_U7pLykHcEeJMZRQWGGu1v_ydtMFw5hXOO2_5XKjD8w8jkRVsB_iWpcfQehP7iOMj9MpozojQ0zOv_3mKtYJClnG5ZRI\"      }   ]}");
			ctx = new ID4meContext("jonh.doe.acme.com", "authority.acme.com", "https://authority.acme.com/");
			client = ID4meClientTest._get_test_client(x => reg_str);
		}

		[Test]
		public void Test0_Good_token()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.trG7rM-fJw-brOzU3oNfENXXb1r9W-eqYsY-pYLH20xm-VnIIMcJ9x7zqKL_bavH3oBOIqLkvErWVg8i4NEld67HfK5fwdlZ9MaA2HJ70bQehqC8WB1T-ybbO6qGI5nLobr-R40t0w39FmstThVmcxhhJlh6k-B0xkIfKsIQ0yG0W4MOibJyRw_WKmhYGvvFPlYI14U0bbvG9IqvgRL8go_rp3Y95kIQItzlibqsswfINRZXzgYmNdw_el3Xef3YucSiKcw0wd9-Vp9fi2xMU44_FwhEiE4QgP0vgm5SmCkmhHUzb8J7NF85cuh7WajWELdV9uWrAkeap_EwBE765Q";
			/*
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1526239022,
				  "exp": 1556239022
				}
			 */
			Assert.DoesNotThrow(
					() => client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks)
				);
		}
		
		[Test]
		public void Test1_Empty_token()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.e30.vvssBSxwPSgjgE3R1oCXBSchRquU6bMesaQ17J7vUIsvGn-O4fDZWgKhG_YGwkJ-eLNAFkhoX6xNLCbmrbJ-b2RPaRRxaMRGvUnKjVCw6rJPaJ5vd-orylGuI2IsM7gZy3kWq0K7hnYmofKDHKhtbQMsZ35GJW9u9kVfxyIdOs-HNaYO7iBWTa92lYnpNgDoGpTY5-BUPqmIzN89NIMUu0Sj4wSy9KLz89bEGN0hMcSNuQi5DvzY61N6WKmHp6grKUBSe0IORd_e5QV1sCFy_EcbrJdrIgnP0bhhj2JypZBqojSw55FgA_7Ki_tPQs9UuJZSueSBaDz_vdhlr80R2w";
			/*
				{
				}
			 */
			var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test1_Empty_token] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test1_Empty_token - no exception");
		}

		[Test]
		public void Test2_iss_not_matching()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL3N1Yi5hdXRob3JpdHkuYWNtZS5jb20vIn0.UkIREX1TWjIhF8m9EKocJ76WBffDj-Gh-gBdn8iZ34XfXwhQa8AIzAsy4LxUkrbjCQMRHWGsCPeA15l1Qv4WmGunrTgzHYfz7SkH8NQAbxrs4N2clX-rOgkptHiMymnm_AT7I3gxJ1CfKsQbq4LedbS_6BGNqSs7qgu8tkWndfq6j6TT7Kitr8JZkK6cuAMVQ2EQa-w-LSjBlt3qnPofhukPJyF0mwRo3owmdyA2Lo14a7avi14GNEZ_0etaT9sijeIazE3okCXpEimBqMMPepzNrR1olatqWY_f3197ttF9oQwtadqX7LMUlGoKDilLLgfEgH_h6B0nwjU3u46OJw";
			/*
				{
				   "iss": "https://sub.authority.acme.com/"
				}
			 */
			var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test2_iss_not_matching] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test2_iss_not_matching - no exception");
		}

		[Test]
		public void Test3_aud_not_matching()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJ3cm9uZ2lkIn0.EnmEAmv7W8mZ2D3JVVzwotnFlyv1UC1Q3NbzcRdL29ePCXvdL0tF971FXe0yFycXSs0RgEFjW95W2IAzkSVu4mi1btDPX2ijpfzedEFJEw5fu0FUXVEYS8NV5jmWdLYrYJr_VXA2kosNxXnD0SAAfrLbwQCm3bytxhA_YRW5slF7eQ8SOg4ynnpsvdibWw0-myT4wqvFxT-CgrbyCySnfxwp8xpSu1ejCLmMPWuntGuQhH5bEH0REarTI2vgN3s61kL3ZcYAX5LC1gThBxxxGkCFYe_NK6TCB5SfGjbLsrGgMn19zYTmjg8yrXMIkYUIckG30YqYR18J7Iw5QE_YZg";
			/*
				{
				   "iss": "https://authority.acme.com/",
				   "aud": "wrongid"
				}
            */
           var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test3_aud_not_matching] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test3_aud_not_matching - no exception");
		}
		
		[Test]
		public void Test4_multi_aud_matching_no_azp()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOlsid3JvbmdpZCIsImxiZ2RrbXk3bGdvNHMiXX0.wy5BAPIzo8of0WwRuNU7zovvXjSUufR-hhsif1WPjw_qQuEuB33zgEYIq-G17Wvg-0sQzOdjZhBOIesHcGrWjyY8_OPKUq6x5y6DfKgbWPooJyB61soRg-erD4Sq4BWFGbQ814WmJUBmVX3i6IBUqyCYm3bTky7BifOhsiFouZ4uycJSE8T_t0ajNo7-Sxm9NiHffMyO6IEppJgRSoxG9ha_FbQgfr2YcE8H5oJCWf4TgF6sJquqoyfb8x71nUBI1P2FFP_2lpPX-5o38YGTzdps0d8DYcd5bH3k_0spl6C-n_EhogrMdlFxpqCLa8zBpnME_kXN2v-gId0DdTbrwg";
			/*
				{
				   "iss": "https://authority.acme.com/",
				   "aud": ["wrongid", "lbgdkmy7lgo4s"]
				}
            */
            var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test4_multi_aud_matching_no_azp] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test4_multi_aud_matching_no_azp - no exception");
		}

		[Test]
		public void Test5_signed_with_other_keys()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTU2MjM5MDIyfQ.S3b-dFbtP2LEhdCNmVTzVwKF4ItG1LXnTr8OyMS4Hsmm-7I-iZEtrK1Iy-zXc_JEC5mUA1T73zoNeDPbSwJAn0OLWWe82tbbohpD0JxbpMuKc6Jv_TBk3l815aNNsHcMFsUgfuI82RiOJ94MeQqzOfvXuyoI6Uwc6JnWu_VDCb4qYa3oTx3CPzDFa9IVjzPwwB7If9h0NDL0bjUS-dyuSl3kffRK6-tr2fK0yDzHYoIIeBoERBowp7iIq5-R8cGgXY_tm4kxKl0w8DBCEHehNWJjV_X4acl1a4Q_xnvKiI_M9dSf-d6g_Yh6euQiLyhFKIJFJRF2ZLLU8XwBX_ZH1g";
			/*
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1556239022
				}
            */
            var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test5_signed_with_other_keys] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test5_signed_with_other_keys - no exception");
			
		}

		[Test]
		public void Test6_signed_with_other_alg()
		{
			var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTU2MjM5MDIyfQ.Woc7qYWGvrTK59ZTrriR1e7pp9aFU6ZT3_gggxgibgw";
			/*
				{
				  "alg": "HS256",
				  "typ": "JWT"
				}
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1556239022
				}
			 */
            var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test6_signed_with_other_alg] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test6_signed_with_other_alg - no exception");
			
		}		

		[Test]
		public void Test7_token_expired()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjJ9.t403-tr7wD-9BcrZxpGTLGUojte_aqDVVOHd4pCC0-_JHUsP8GysNs3r-Xjsf8ATbAYkZvYMgzRI9iwT4lMuIIsqWFvq17Odp74WitnMc0da6Vlhsq9S7jEloUNyZchXMU0jQXdp4__moDZJCTs5_ba3ZGKAfXKx_kYAbT5iunc1X2agmKNUL-EPTqK0UBd8TVIGTi4ZwkW9pFlkZnaC6DscIkbNmDEvmZ1K0uzYwwVzARHzS3S7oyKlJiv4Mm7Gi5N1yuKbLwc20bLHCdkTMkL1BJPrK_ygYe3tyocMUeFQ6uRY1mLRNWyOb8xlf98OIoP1OaLWAdmMmNBMA43ZwA";
			/*
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022
				}
			 */
            var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(0, 0, 5, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test7_token_expired] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test7_token_expired - no exception");
			
		}		

		[Test]
		public void Test8_nonce_missing()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjJ9.trG7rM-fJw-brOzU3oNfENXXb1r9W-eqYsY-pYLH20xm-VnIIMcJ9x7zqKL_bavH3oBOIqLkvErWVg8i4NEld67HfK5fwdlZ9MaA2HJ70bQehqC8WB1T-ybbO6qGI5nLobr-R40t0w39FmstThVmcxhhJlh6k-B0xkIfKsIQ0yG0W4MOibJyRw_WKmhYGvvFPlYI14U0bbvG9IqvgRL8go_rp3Y95kIQItzlibqsswfINRZXzgYmNdw_el3Xef3YucSiKcw0wd9-Vp9fi2xMU44_FwhEiE4QgP0vgm5SmCkmhHUzb8J7NF85cuh7WajWELdV9uWrAkeap_EwBE765Q";
			/*
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022
				}
			 */
			ctx.nonce = Guid.NewGuid().ToString();
			try
			{
	            var thrown = false;
				try {
					var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
				}
				catch (ID4meTokenException e) {
					Console.WriteLine(string.Format("[Test8_nonce_missing] Expected exception occured: {0}", e.Message));
					thrown = true;
				}
				Assert.IsTrue(thrown, "Test8_nonce_missing - no exception");
			}
			finally
			{
				ctx.nonce = null;
			}			
		}		

		[Test]
		public void Test9_nonce_incorrect()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjIsIm5vbmNlIjoiRjkxOURGNTktQzc3MS00MDFGLTlGQTAtREIwODdCNjU3OUFDIn0.k-utZJpsp6mycHEXCjY_AWG3npI-VOEcMHvU1dewGQE5rdjLR2OzDH60NQpdWlw8p-2MtEMPrG7X8VW24AnY-Sl_cFuczxV9PxXC_kVfPQKmBqmgv15lqvj58KSK6VtREcgtDBWuL0NJshBs_esWaHZSc6HKtMvFOXoByBgWjVK4ulMniADfYcQ6YzWC2biyKNTMvRsJUdj1bOshg-9JpeIJ5Jya6ypGQtfZn7ke1hsNr-dL7dyOwLP-bRMHM4aF-KX9HBBXQfkTKcUpRpXjAmOx5Q7wVNyttn3XEu-4Nv5nnSeVi-e28go7VBsyIvCRAlVj0-immfBBrWTHJAzH-A";
			/*
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022,
					"nonce": "F919DF59-C771-401F-9FA0-DB087B6579AC"
				}
			 */			
			ctx.nonce = Guid.NewGuid().ToString();
			try
			{
	            var thrown = false;
				try {
					var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
				}
				catch (ID4meTokenException e) {
					Console.WriteLine(string.Format("[Test9_nonce_incorrect] Expected exception occured: {0}", e.Message));
					thrown = true;
				}
				Assert.IsTrue(thrown, "Test9_nonce_incorrect - no exception");
			}
			finally
			{
				ctx.nonce = null;
			}						
		}		

		[Test]
		public void Test10_nonce_replay()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1MzYyMzkwMjIsIm5vbmNlIjoiRjkxOURGNTktQzc3MS00MDFGLTlGQTAtREIwODdCNjU3OUFDIn0.k-utZJpsp6mycHEXCjY_AWG3npI-VOEcMHvU1dewGQE5rdjLR2OzDH60NQpdWlw8p-2MtEMPrG7X8VW24AnY-Sl_cFuczxV9PxXC_kVfPQKmBqmgv15lqvj58KSK6VtREcgtDBWuL0NJshBs_esWaHZSc6HKtMvFOXoByBgWjVK4ulMniADfYcQ6YzWC2biyKNTMvRsJUdj1bOshg-9JpeIJ5Jya6ypGQtfZn7ke1hsNr-dL7dyOwLP-bRMHM4aF-KX9HBBXQfkTKcUpRpXjAmOx5Q7wVNyttn3XEu-4Nv5nnSeVi-e28go7VBsyIvCRAlVj0-immfBBrWTHJAzH-A";
			/*
				{
					"iss": "https://authority.acme.com/",
					"aud": "lbgdkmy7lgo4s",
					"iat": 1526239022,
					"exp": 1536239022,
					"nonce": "F919DF59-C771-401F-9FA0-DB087B6579AC"
				}
			 */
			ctx.nonce = "F919DF59-C771-401F-9FA0-DB087B6579AC";
			try
			{
	            var thrown = false;
				try {
					var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
					client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(100 * 365, 0, 0, 0), true, x => auth_jwks);
				}
				catch (ID4meTokenException e) {
					Console.WriteLine(string.Format("[Test10_nonce_replay] Expected exception occured: {0}", e.Message));
					thrown = true;
				}
				Assert.IsTrue(thrown, "Test10_nonce_replay - no exception");
			}
			finally
			{
				ctx.nonce = null;
			}						
		}

		[Test]
		public void Test11_ID4me_Identifier_Mismatch()
		{
			var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2F1dGhvcml0eS5hY21lLmNvbS8iLCJhdWQiOiJsYmdka215N2xnbzRzIiwiaWF0IjoxNTI2MjM5MDIyLCJleHAiOjE1NTYyMzkwMjIsImlkNG1lLmlkZW50aWZpZXIiOiJ3cm9uZy5kb2UuYWNtZS5jb20ifQ.BjGLOgYnMbTrnfdRJUK1QvPMNXy27b64SAxmdxX6V1-6SX2fn5QD_YpPod8_ImPAN01c4AjDTH4O1jLQ5o1MPrffdWkRn9mp3E2rUvK3ivXODpdYEkGbwfW3huphn4kQc1U5CEw1LsRc9odVs_oubtmA-SMgMpizZOL2WrQIALWy-QbxlXKfm_PsiCAGRLcZW3WBMflzec0UW9jOSv4DoT-ApRuInKUSmGHi_S7PqWqYsMOZp0dilFQpfrWx6IlcGdncIJvT0aBeuO_Z_RNqP4iUJJAvytVayIUlaFCZFUVVaelrWe_O9d1zz-gkWmj0OWjgvOt2GxfSr2O2bBr8WQ";
			/*
				{
				  "iss": "https://authority.acme.com/",
				  "aud": "lbgdkmy7lgo4s",
				  "iat": 1526239022,
				  "exp": 1556239022,
				  "id4me.identifier": "wrong.doe.acme.com"
				}
			 */

            var thrown = false;
			try {
				var claims = client._decode_token(token, ctx, ctx.iau, ID4meClient.TokenDecodeType.IDToken, new TimeSpan(0, 0, 5, 0), true, x => auth_jwks);
			}
			catch (ID4meTokenException e) {
				Console.WriteLine(string.Format("[Test11_ID4me_Identifier_Mismatch] Expected exception occured: {0}", e.Message));
				thrown = true;
			}
			Assert.IsTrue(thrown, "Test11_ID4me_Identifier_Mismatch - no exception");
		}
	}
}


