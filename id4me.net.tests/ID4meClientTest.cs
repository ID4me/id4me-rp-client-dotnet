﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 00:40
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using ID4me.Jose.jwk;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using org.id4me.Exceptions;
using Newtonsoft.Json;
using TestHttpServer;
using System.Security.Cryptography;

namespace org.id4me
{
	[TestFixture]
	public class ID4meClientTest
	{
		private const string test_host = "localhost";
		private const int test_port = 8090;
		private const string callbackPath = "/validate";

		internal static ID4meClient _get_test_client(
			Func<string, string> find_authority, 
	    	Action<string, string> save_authority = null,
			Dictionary<string, object> parameters = null,
			string private_jwks = null)
		{
			if (parameters == null)
				parameters = new Dictionary<string, object>();
	        return new ID4meClient(
				string.Format("http://{0}:{1}{2}", ID4meClientTest.test_host, ID4meClientTest.test_port, ID4meClientTest.callbackPath),
	            "Test Client", 
				find_authority, save_authority,
	            null,
				OIDCApplicationType.native,
	            "test_client_id",
				string.Format("http://{0}:{1}/logo.png", ID4meClientTest.test_host, ID4meClientTest.test_port),
	            string.Format("http://{0}:{1}/about", ID4meClientTest.test_host, ID4meClientTest.test_port),
				string.Format("http://{0}:{1}/documents", ID4meClientTest.test_host, ID4meClientTest.test_port),
				private_jwks,
				parameters.ContainsKey("requireencryption") ? (bool?) parameters["requireencryption"] : (bool?) null
			);			
		}

		[Test]
		public void Test_get_identity_authority()
		{
			// happy case
			string auth = null; 			
			Assert.DoesNotThrow(
				() => auth = ID4meClient._get_identity_authority("idtest1.domainid.community")
			);
			Assert.IsNotNull(auth);
			Assert.IsNotEmpty(auth);
			Assert.AreEqual("id.test.denic.de", auth);

			// not existing ID
			Assert.Throws(
				typeof(ID4meDNSResolverException),
			    () => auth = ID4meClient._get_identity_authority("notthere.domainid.community")
			);
		}

		[Test]
		public void Test_get_identity_authority_parent_domain()
		{
			// happy case
			string auth = null; 			
			Assert.DoesNotThrow(
				() => auth = ID4meClient._get_identity_authority("joe-doe.mojeid.cz")
			);
			Assert.IsNotNull(auth);
			Assert.IsNotEmpty(auth);
			Assert.AreEqual("mojeid.cz/oidc", auth);
		}		
		
		[Test]
		public void Test_get_openid_configuration()
		{
			// happy case
			JObject conf = null; 			
			
			var client = new ID4meClient("http://localhost:8080", "ID4me.NET test", null);
			
			Assert.DoesNotThrow(
				() => conf = client._get_openid_configuration("id.denic.de")
			);
			Assert.IsNotNull(conf);
			Assert.AreEqual("https://id.denic.de", (string) conf["issuer"]);
		}
		
		[Test]
		public void Test_generate_new_private_keys_set()
		{
			var jwks = ID4meClient.Generate_new_private_keys_set();
			Assert.IsInstanceOf(typeof(List<JWK>), jwks);
		}

		[Test]
		public void Test_register_identity_authority()
		{			
			var client = new ID4meClient("http://localhost:8080", "ID4me.NET test", x => null,
			                             null, null, 
			                             OIDCApplicationType.native, null, 
			                             null, null, null, 
			                             JWKS.Serialize(ID4meClient.Generate_new_private_keys_set()));
			
			// happy path
			JObject client_reg = null;
			Assert.DoesNotThrow(
				() => client_reg = client._register_identity_authority("id.denic.de")
			);
			Assert.IsTrue(client_reg.ContainsKey("client_id") && !string.IsNullOrEmpty(client_reg["client_id"].ToString()), "client_id not delivered in client registration response");
			Assert.IsTrue(client_reg.ContainsKey("client_secret") && !string.IsNullOrEmpty(client_reg["client_secret"].ToString()), "client_id not delivered in client registration response");
			Assert.IsFalse(client_reg.ContainsKey("private_jwks"), "private_jwks delivered in client registration response");

			// not existing Authority
			Assert.Throws(typeof(ID4meAuthorityConfigurationException),
				() => client_reg = client._register_identity_authority("noid.denic.de")
			);
		}

		[Test]
		public void Test_transform_keys()
		{
			var orgkeys = ID4meClient.Generate_new_private_keys_set();
			var private_jwks_str = JWKS.Serialize(orgkeys, true);
			var private_jwks = JWKS.Parse(private_jwks_str);
			var private_jwks_list = new List<JWK>();
			foreach (var k in private_jwks)
				private_jwks_list.Add(k);
			var serializedpubkey = JWKS.Serialize(private_jwks_list, false);
			var json_jwks = JsonConvert.DeserializeObject(serializedpubkey);
		}
		
		[Test]
		public void Test_get_rp_context()
		{			
			var authStore = new Dictionary<string, string>();

			var client = new ID4meClient("http://localhost:8080", "ID4me.NET test", 
				auth => authStore.ContainsKey(auth) ? authStore[auth] : null,
				(auth, val) => authStore[auth] = val);			
			
			var ctx = client.get_rp_context("id200.connect.domains");
			
			Assert.IsNotNull(ctx, "Returned context is empty");
			Assert.IsTrue(authStore.ContainsKey("id.test.denic.de"), "Auth registration not cached");
			Assert.IsNotEmpty(ctx.iau, "ctx.iau is empty");
			Assert.IsNotEmpty(ctx.id, "ctx.id is empty");
			
			var str = JsonConvert.SerializeObject(ctx);
			Assert.IsFalse(str.Contains("\"private_jwks\""));
		}

		[Test]
		public void Test_reregister_expired_client_registration()
		{			
				var authStore = new Dictionary<string, string>()
			{
				{ "id.test.denic.de", "{\"token_endpoint_auth_method\": \"client_secret_basic\", \"client_id_issued_at\": 1538748629, \"subject_type\": \"public\", \"application_type\": \"native\", \"registration_access_token\": \"doO0I2uYF8XQf2pNroAZEQQ303EzB4DHNxNhsOEWMNA.YSxpLHI\", \"id_token_encrypted_response_alg\": \"RSA-OAEP-256\", \"userinfo_encrypted_response_alg\": \"RSA-OAEP-256\", \"jwks\": {\"keys\": [{\"n\": \"1kieZdE6xwRm9-n-xGdFLp9r_e3rt38nVEHz8q-AWYOd17fnSZzJqx85LLSBTfhd44GglMXY44wuTEOgqy0WATIKopGRCiPE0U9J34nUpbz4xDulGbREstqmaCjq23hYCKmynjLqfrLM2M1tK6cDeWOYnVEIgcZPpaTutjBXBu6z7anfOVOlpeDfM61UJLwYMonWPvIadCBL0hcBodH_1fKt2w1BiUC_ml5Pb8gXONWFPXR3VK-zWkGj4TuOwrDhgaUkpL_zl6BBWhecNlyZjsNnIPwR44fl4ngQeJ7K0G0HuGDhjEb9cVL1qxdpBW0ZJXkBlDUBNbeTWCzOkvcv9Q\", \"e\": \"AQAB\", \"kty\": \"RSA\", \"kid\": \"63bd23e0-563f-4cb9-8139-538c6a704ad9\"}]}, \"client_name\": \"Test\", \"grant_types\": [\"authorization_code\"], \"userinfo_signed_response_alg\": \"RS256\", \"id_token_signed_response_alg\": \"RS256\", \"response_types\": [\"code\"], \"tos_uri\": \"https://id4me.org/documents\", \"client_secret_expires_at\": 1538748629, \"redirect_uris\": [\"http://localhost:8090/validate\"], \"client_id\": \"iue5k7yuqgskg\", \"policy_uri\": \"https://id4me.org/about\", \"id_token_encrypted_response_enc\": \"A128CBC-HS256\", \"tls_client_certificate_bound_access_tokens\": false, \"userinfo_encrypted_response_enc\": \"A128CBC-HS256\", \"registration_client_uri\": \"https://auth.freedom-id.de/clients/iue5k7yuqgskg\", \"logo_uri\": \"https://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png\", \"client_secret\": \"inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE\"}"}
			};

			var client = new ID4meClient("http://localhost:8080", "ID4me.NET test", 
				auth => authStore.ContainsKey(auth) ? authStore[auth] : null,
				(auth, val) => authStore[auth] = val);
			
			var ctx = client.get_rp_context("id200.connect.domains");
			
			Assert.IsNotNull(ctx, "Returned context is empty");
			Assert.IsTrue(authStore.ContainsKey("id.test.denic.de"), "Auth registration not cached");
			Assert.IsFalse(authStore["id.test.denic.de"].Contains("iue5k7yuqgskg"), "Expected new client ID as the old expired");

			var authStore2 = new Dictionary<string, string>()
			{
				{ "id.test.denic.de", "{\"token_endpoint_auth_method\": \"client_secret_basic\", \"client_id_issued_at\": 1538748629, \"subject_type\": \"public\", \"application_type\": \"native\", \"registration_access_token\": \"doO0I2uYF8XQf2pNroAZEQQ303EzB4DHNxNhsOEWMNA.YSxpLHI\", \"id_token_encrypted_response_alg\": \"RSA-OAEP-256\", \"userinfo_encrypted_response_alg\": \"RSA-OAEP-256\", \"jwks\": {\"keys\": [{\"n\": \"1kieZdE6xwRm9-n-xGdFLp9r_e3rt38nVEHz8q-AWYOd17fnSZzJqx85LLSBTfhd44GglMXY44wuTEOgqy0WATIKopGRCiPE0U9J34nUpbz4xDulGbREstqmaCjq23hYCKmynjLqfrLM2M1tK6cDeWOYnVEIgcZPpaTutjBXBu6z7anfOVOlpeDfM61UJLwYMonWPvIadCBL0hcBodH_1fKt2w1BiUC_ml5Pb8gXONWFPXR3VK-zWkGj4TuOwrDhgaUkpL_zl6BBWhecNlyZjsNnIPwR44fl4ngQeJ7K0G0HuGDhjEb9cVL1qxdpBW0ZJXkBlDUBNbeTWCzOkvcv9Q\", \"e\": \"AQAB\", \"kty\": \"RSA\", \"kid\": \"63bd23e0-563f-4cb9-8139-538c6a704ad9\"}]}, \"client_name\": \"Test\", \"grant_types\": [\"authorization_code\"], \"userinfo_signed_response_alg\": \"RS256\", \"id_token_signed_response_alg\": \"RS256\", \"response_types\": [\"code\"], \"tos_uri\": \"https://id4me.org/documents\", \"client_secret_expires_at\": 0, \"redirect_uris\": [\"http://localhost:8090/validate\"], \"client_id\": \"iue5k7yuqgskg\", \"policy_uri\": \"https://id4me.org/about\", \"id_token_encrypted_response_enc\": \"A128CBC-HS256\", \"tls_client_certificate_bound_access_tokens\": false, \"userinfo_encrypted_response_enc\": \"A128CBC-HS256\", \"registration_client_uri\": \"https://auth.freedom-id.de/clients/iue5k7yuqgskg\", \"logo_uri\": \"https://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png\", \"client_secret\": \"inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE\"}"}
			};

			var client2 = new ID4meClient("http://localhost:8080", "ID4me.NET test", 
				auth => authStore2.ContainsKey(auth) ? authStore2[auth] : null,
				(auth, val) => authStore2[auth] = val);
			
			var ctx2 = client2.get_rp_context("id200.connect.domains");
			
			Assert.IsNotNull(ctx2, "Returned context is empty");
			Assert.IsTrue(authStore2.ContainsKey("id.test.denic.de"), "Auth registration not cached");
			Assert.IsTrue(authStore2["id.test.denic.de"].Contains("iue5k7yuqgskg"), "Expected old client ID as not expired");


			var authStore3 = new Dictionary<string, string>()
			{
				{ "id.test.denic.de", "{\"token_endpoint_auth_method\": \"client_secret_basic\", \"client_id_issued_at\": 1538748629, \"subject_type\": \"public\", \"application_type\": \"native\", \"registration_access_token\": \"doO0I2uYF8XQf2pNroAZEQQ303EzB4DHNxNhsOEWMNA.YSxpLHI\", \"id_token_encrypted_response_alg\": \"RSA-OAEP-256\", \"userinfo_encrypted_response_alg\": \"RSA-OAEP-256\", \"jwks\": {\"keys\": [{\"n\": \"1kieZdE6xwRm9-n-xGdFLp9r_e3rt38nVEHz8q-AWYOd17fnSZzJqx85LLSBTfhd44GglMXY44wuTEOgqy0WATIKopGRCiPE0U9J34nUpbz4xDulGbREstqmaCjq23hYCKmynjLqfrLM2M1tK6cDeWOYnVEIgcZPpaTutjBXBu6z7anfOVOlpeDfM61UJLwYMonWPvIadCBL0hcBodH_1fKt2w1BiUC_ml5Pb8gXONWFPXR3VK-zWkGj4TuOwrDhgaUkpL_zl6BBWhecNlyZjsNnIPwR44fl4ngQeJ7K0G0HuGDhjEb9cVL1qxdpBW0ZJXkBlDUBNbeTWCzOkvcv9Q\", \"e\": \"AQAB\", \"kty\": \"RSA\", \"kid\": \"63bd23e0-563f-4cb9-8139-538c6a704ad9\"}]}, \"client_name\": \"Test\", \"grant_types\": [\"authorization_code\"], \"userinfo_signed_response_alg\": \"RS256\", \"id_token_signed_response_alg\": \"RS256\", \"response_types\": [\"code\"], \"tos_uri\": \"https://id4me.org/documents\", \"redirect_uris\": [\"http://localhost:8090/validate\"], \"client_id\": \"iue5k7yuqgskg\", \"policy_uri\": \"https://id4me.org/about\", \"id_token_encrypted_response_enc\": \"A128CBC-HS256\", \"tls_client_certificate_bound_access_tokens\": false, \"userinfo_encrypted_response_enc\": \"A128CBC-HS256\", \"registration_client_uri\": \"https://auth.freedom-id.de/clients/iue5k7yuqgskg\", \"logo_uri\": \"https://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png\", \"client_secret\": \"inUXSzOZDEtG0TsMqBOuMIyHODHRmML24_QdjCoxaWE\"}"}
			};
			var client3 = new ID4meClient("http://localhost:8080", "ID4me.NET test",
				auth => authStore3.ContainsKey(auth) ? authStore3[auth] : null,
				(auth, val) => authStore3[auth] = val);
			
			var ctx3 = client3.get_rp_context("id200.connect.domains");
			
			Assert.IsNotNull(ctx3, "Returned context is empty");
			Assert.IsTrue(authStore3.ContainsKey("id.test.denic.de"), "Auth registration not cached");
			Assert.IsTrue(authStore3["id.test.denic.de"].Contains("iue5k7yuqgskg"), "Expected old client ID as not expired");
		}
		
	    public static IEnumerable Test_get_user_info_data
	    {
	        get
	        {
	        	yield return new TestCaseData("idtest1.domainid.community", null)
	        		.SetName("DENIC ID 1");				
	        	yield return new TestCaseData("idtest1.domainid.community", false)
   	        		.SetName("DENIC ID 2");
	        	yield return new TestCaseData("doe-john.mojeid.cz", false)
	        		.SetName("MojeID ID 1");
	        }
	    }  
		
		[Test, TestCaseSource("Test_get_user_info_data")]
		public void Test_get_user_info(string test_id, bool? enableEncryption)
		{
			var test_state = "state_" + (new Random((int) (DateTime.Now.Ticks % int.MaxValue))).Next();
			
			var authStore = new Dictionary<string, string>();

			var client = _get_test_client(auth => authStore[auth],
			                                (auth, config) => authStore[auth] = config,
			                                new Dictionary<string, object>{{"requireencryption", enableEncryption} },
			                                JWKS.Serialize(ID4meClient.Generate_new_private_keys_set(), includePrivateParameters: true));
						
			var ctx = client.get_rp_context(test_id);
				
			var link = client.get_consent_url(
				ctx, test_state,
				new ID4meClaimsRequest {
					id_token = new Dictionary<string, OIDCClaimRequestOptions>()
					{
						{ 	OIDCClaimName.email, new OIDCClaimRequestOptions("Test reason", true)},
						{ 	OIDCClaimName.email_verified, new OIDCClaimRequestOptions()}
					},
					userinfo = new Dictionary<string, OIDCClaimRequestOptions>()
					{
						{ 	OIDCClaimName.email, new OIDCClaimRequestOptions("Test other confusing reason", true)},
						{ 	OIDCClaimName.address, new OIDCClaimRequestOptions("Yet another reason", true)},
						{ 	OIDCClaimName.birthdate, new OIDCClaimRequestOptions(null, true)}						
					}
				},
				OIDCLoginPrompt.login,
				true);
			Assert.IsNotEmpty(link, "Login URL is empty");
				
			System.Diagnostics.Process.Start(link);
			
			var testServer = new TestHttpServer.TestHttpServer(ID4meClientTest.test_host, ID4meClientTest.test_port);
			Uri verifyCall = null;
			testServer.Listen(uri => {
			                  	if (uri.LocalPath == ID4meClientTest.callbackPath)
			                  	{
			                  		verifyCall = uri;
			                  		return true;
			                  	}
			                  	return false;
			                  });
			
			
			Assert.IsNotNull(verifyCall, "No callback Uri assigned");
			Assert.AreEqual(ID4meClientTest.callbackPath, verifyCall.LocalPath, "Wrong end-point called");
			var queryParams = HttpUtility.ParseQueryString(verifyCall.Query);
			Assert.Contains("code", queryParams.Keys, "Code not passed back");
			Assert.Contains("state", queryParams.Keys, "State not passed back");
			Assert.AreEqual(test_state, queryParams["state"], 
			                string.Format("Different test_state coming back {0} {1}", test_state, queryParams["state"]));
			

			var id_token = client.get_idtoken(ctx, queryParams["code"]);
			Assert.IsNotNull(id_token, "ID token empty");
			Console.WriteLine(string.Format("** ID token: \n{0}",
			                                JsonConvert.SerializeObject(id_token, Formatting.Indented)));
			Assert.IsTrue(id_token.ContainsKey("iss"), "No iss in id token");
			Assert.IsTrue(id_token.ContainsKey("sub"), "No sub in id token");
			Assert.IsTrue(id_token.ContainsKey("nonce"), "No nonce in id token");

			var user_info = client.get_user_info(ctx);
			
			Assert.IsNotNull(user_info, "User info empty");
			Console.WriteLine(string.Format("** User Info: \n{0}",
			                                JsonConvert.SerializeObject(user_info, Formatting.Indented)));
			Assert.IsTrue(user_info.ContainsKey("iss"), "No iss in user info");
			Assert.IsTrue(user_info.ContainsKey("sub"), "No sub in user info");
			Assert.IsTrue(user_info.ContainsKey(OIDCClaimName.email), "No email in user info");
			Assert.IsTrue(user_info.ContainsKey(OIDCClaimName.address), "No address in user info");
			Assert.IsTrue(user_info.ContainsKey(OIDCClaimName.birthdate), "No birthdate in user info");			
		}
		
		[Test]
		public void Test_decode_token()
		{
			var access_token = "eyJraWQiOiJGT3Z5IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJyc0xYM1hwb0RIbkd4WHQrekZsNVZTOWMwdmE1RUJkdTQ4b3BHY2FITVI0dmFGZHBMdnE0Yys3XC9aY0FIdENzWSIsImlkNG1lLmlkZW50aWZpZXIiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJpZGVudGlmaWVyIjoiam9obi5kb2UuZG9tYWluaWQuY29tbXVuaXR5IiwiaWQ0bWUiOiJqb2huLmRvZS5kb21haW5pZC5jb21tdW5pdHkiLCJjbG0iOlsiZW1haWxfdmVyaWZpZWQiLCJuaWNrbmFtZSIsImdpdmVuX25hbWUiLCJlbWFpbCIsInBpY3R1cmUiXSwic2NvcGUiOlsib3BlbmlkIl0sImlzcyI6Imh0dHBzOlwvXC9pZC50ZXN0LmRlbmljLmRlIiwiZXhwIjoxNTUyMzE0ODY2LCJpYXQiOjE1NTIzMTQyNjYsImp0aSI6IjJveS1EVGNJNjZJIiwiY2xpZW50X2lkIjoiM3ptY2U1YmNzYnh2bSJ9.cETdnolV8fV5ovyZvAj51Oho7zcqXn7RDy3AZTSmDscNsdSlwY_6czKz7hmQJtLFH-T8S3Jj2Acz_3FPGhcH8fWOOtNf5femWK-4BMuCz7KewtKy2yCKza1vubgk6-nMrIo-46zKLiwFfroBMhMr-KgEkg2RJSJOoSoN9Nt-OiIMjekYQaK9U_XamFQXcQ6tsSeCyhtrpImTXU985T-uCRCiurUNR6pDcq5agcofuAMggZ_ZSUigf01WNGPh59HUXAUO2efcPBGug9AXmYpPkWz4A0lELd4hbEZuNTZVqwJy3ptoLSycaMiLM5OO0wG19Bbtr3yZ5K0VHA_Xoyqbzg";
			var id_token = "eyJraWQiOiI5NjY0YTkzNS1jNDlkLTQ5MTQtOTYzYS0xMTBhYmE1ZDYyMDIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.PmOUU57YXm_qWGFMYmnQ-bnK50aD7bnYPlQAOLodvrLvoLbZA6O02PM8PKBo-tFmLGR6KAUvHw_7gUqm-ynWfcerqlzM6Z0D1eQColUxMmk1F3rnW9Jw9iZx_B11YivTASrU7IaJ_5p6_U3y041K36HDPEg4ma-95LF6TNj7FYPH0_NrxsBO7gMXGMKY67KUuvkFEu-X2seV-xrp8bBQ9E9sgKMpRLKVQCkZmiznMXx2TLam354tnh7c7Q6mZnmmV-Nue1F0RbN7YS1QbDGErH17NVVEHft27SwCq_kLOzgsRNgRR5fyHGwFV1beSpk3iQDNoR6lam2lwl0ndj76Jg.FSahbkHluSfyD-ALtD3g4g.NsfVoQfHYTPsXtd1FrEzmm05uyqp0UbfBUao88s2maaf6t3t_uHaL3SoVFS8rs4UCk9VqSCAYGgUJPEhwpJkVkywCEHgBalCLS6lgxvv9wLESL4jMeWm9sMdUrCVl7S8TBwpU9HFiBdvOmw2iOeG1AZ8NzP5ohCPkdNhbrgfmv5C0sHArJjx5Gi0hMshWrYOG6rHsch2t_b0QNmDFOtrPyMzbEPFD7W9GbONUaCzv1K-YADxzhDHH_uUGajlj5qTSb5kIPXOhzmMI4FbRvqBO-76UYmcgZTyw71d6vRNG6S6uHuwOWfCCQSTNi3EeBeimRjN_tX1J2Tw_WOAZW8o_UgdcPYunEjfnnqo8yOeYY75XGzHHDiMj5zhXuWe_ZITRTDt8OoUUkA91Zhr1OLyVwg1-vk9E0J4NhuXFCT7ND4_r21CCiZ46g6ofQHr9Z2UR6j-oR7dS75d0Z6iju57zHbJScXSkVgb9AgtmIFGaXdP3xk7Osc1f34_A3gnsmJDfsakXsMgHh335G-OLvpoIn7eeWFMxdh-yODp81OX6NLFbtmZG0DweG1o8EIkvtm1vYcLDQCAH4TUQeqQHkVA18_5IiyIarG8XMX8Oh1QURpgRFXJjleRebg5ULlOzBhWtqH_UT6kh9jN5WJi7aaV82q7bCK8cj2sEqDsQua-aaejL_MhNAjEXStD7eJjtJJt_1aZRVRV4jV-IxjvRsrVxAheMW4nAed91BebP1EOh2Gr_6JzKyaYl0GQZISoDses0f3bHknc7Ahq3NlvWv9jD8uGBHGatM73ibOjrcTSVyq57XUp8l1JbNEFqU2K_WTcxlG-OKthKexUirlQXPxg9T7I6I_2jJfiVXW5e6Nh8pcv0nV6VIjOIqnGbWZSjeQthL7aRWnh1b4zPCrNnqp9ZoUO7_Mmb8WkV0qIupgR8j3FNrdDTK8SpE36ghOoTjGL.eN9PXvRIYhqcZA1tCS5sqg";
			var user_info_agent = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJyc0xYM1hwb0RIbkd4WHQrekZsNVZTOWMwdmE1RUJkdTQ4b3BHY2FITVI0dmFGZHBMdnE0Yys3L1pjQUh0Q3NZIiwiaWQ0bWUuaWRlbnRpZmllciI6ImpvaG4uZG9lLmRvbWFpbmlkLmNvbW11bml0eSIsImlkNG1lLmlkZW50aXR5Ijoiam9obi5kb2UuZG9tYWluaWQuY29tbXVuaXR5IiwiZXhwIjoxNTUyMzE2NTk5LCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaWF0IjoxNTUyMzE2Mjk5LCJnaXZlbl9uYW1lIjoiR2l2ZW5OYW1lIiwidXBkYXRlZF9hdCI6MTU1MjMxNjI5MCwiaXNzIjoiaHR0cHM6Ly9pZGVudGl0eWFnZW50LmRlIiwibmJmIjoxNTUyMzE2Mjk5LCJlbWFpbCI6Im1haWxAdGVzdC5kZSIsIm5pY2tuYW1lIjoiTmlja25hbWUiLCJhdWQiOiIzem1jZTViY3NieHZtIiwicGljdHVyZSI6Imh0dHA6Ly9waWN0dXJlLmNvbS9waWN0dXJlIn0.G6WaEjieFb8xndfHYMpZ8cXqT5eAUATPm4drwlNolsav7Va-Jto-KUnmmNG4glJUcwHyK8-HkY7MbjKwvtBPQbj3y8r4GcSNlE3l1tE6WGuf1SUu0S8AOhQ-p_cvutxIjju0VHg3XvtfPG5yFnb8GXmuoC89Y6SmhXaI_gb5PUufLA0fJcNlO8bQqsK74mUxAh1aI_OlZ4TMOLNTo6a80w6xGvqFSPIZs5ok7ErxLb8Ku8lLBKfdwCS_hXpii0IoDwT9OJUmAuL9iB88y-O0omIv7qqoHOFXuh1n8N84c1qFwK3YApzSQObmsN88s9_m2pFj4EsYNZIOGelQ4JEx9A";

			var jwks = "{\"keys\":[{\"d\":\"LiY9nb5v4NW8-iBtU3Z_Bx-PNFzX57qeaEqh2AZuuvFLZpBNvKhELJ0_Re308LweyCYVJHaP6MUrVs9i2PwGTtNxxuZ835K09T_9wc5OcNIjzI5WxLc8j1Wwd65v1QlndmXh4MS9rPjPC3us4modamFcjf5I0Lg9o-0miCt77SYROl_ZuHCX8QVCVrySU4UkU9xpZONpkDn25CpwnrftRhO1hP60HIiXWA6vQvYYHpRX9f5wuqIshcoMzXccSHf_TMY9UEAsxpvIqO98eRCxWNF75RTBynddqoSWVBnP_JwCUWOG3ZtwS7EGYd5HncNKdramrh0XK-JOzS6xb7UyzQ\",\"dp\":\"I6knDqeSJTdbYw-OfXM2KmqgUZ-z6QaVqpLDPyNeTfJJTwklH1i9T5i_6t849fOKBOjrYhZ-Poz3t1tVJ7n77_colyr-i7o3irMoVnMI-m8EalwVGV61jJ7BlAruhnvrINNrGauUJ9E_2kkcFaJtlQy3LPN2r8e0oX2qH9NANkk\",\"dq\":\"DfKB5JouG30jiVnFJljMwMKTJw47o4bSJ5BGqZIxfNjVgtQir2-_Ekzh4F_kVMxFIDhSc-gKBFtDw1SMkbfZUZl8gdhZe9ejvhXuh08NuTKDbdayuWNCv4gGd7kwTIvjjI545uaLsc5fql7-D9Ai6gF40lfzZZp9If0yUve8CAM\",\"e\":\"AQAB\",\"kid\":\"9664a935-c49d-4914-963a-110aba5d6202\",\"kty\":\"RSA\",\"n\":\"saZoYnYQBKfWQY0Uy0stB3tpRVXCJce0YQhRPPpfYizQhueo-2I_UVrdenZhzyWKFg6LfR2z_pKcDPj5A3TepXXSWo4X8HnOKu3V9E1pplwfzE5NEGADd4-Fvfjs6RZtX8JLIhj3teBVO4LXBryHGOW-s24_DIqdItoZbpCvRwZaHskZG0ruWIX27hSCNQyxygLNRmbWRBYo0sHloOFPV2Yl3aM5N2VOTEfhkJr4346GHOGDmrDgirC3DdjpEqBmk1cQXP2T3J4EmMpZOsn-RiuRsjvBHIyRr9mooKYL2ekAmr8CD5PVeSslntf_F-_w4ATv4Ld2MLsXS7mrl41DBQ\",\"p\":\"5FJM0z-LeWKNtz7DZsheYKhViieNxgor2JhnsCJZd8L1IPlmwJ8tueyUjG7wbFrVsdfKp4Mo15CDXAMdMBjmvSa6oY1_hJIjiWvRbAbJ0MgMo60nc6Pv4H7N807Lg9AFhVDUaQIL47ldOYvp9cJBnNGWZoGKop-SllDsB4mN_m8\",\"q\":\"xy-TgNAeqeF8mLPE37KE99-jtcVaMR3wZsfytumwZW-lm8oVgmUcnt_u4157_ni3OidPgdcpItNvGi5L4h9MOmWJI7Hc3aSZnj7ZTFjeQ4OFg7eCmUxENtMXseGK1eRyfzE6e-y7PGtp3eVbz-yD_yED81LwTKSbx6zJD3S0D8s\",\"qi\":\"tKkmN7wcmG63hAQAoq9oD0KYXjv6V5bnrr5NBHHWtO_fqHpofqhHizYemmuQnvad8I7kg6h1iN_abhxMaE5gtUi-kzdRZy1hx_KU3w5XO2F6h60WKfQEuXtIkPFaGjtLPhzGTDDaisOsOYltKQOIuUkjaO3ZBatzGSi9BLLeqT8\"}]}";
	
			var reg_str = @"{
		            ""client_id"": ""3zmce5bcsbxvm"",
		            ""client_secret"": ""9KpB_rg-VZp7_s1di4zABocV22hCdfzKJxGuMpvfolk""}";

            var ctx = new ID4meContext(
                "john.doe.domainid.community",
                "id.test.denic.de",
                "https://id.test.denic.de")
            {
                access_token = access_token
            };

            var client = ID4meClientTest._get_test_client(x => reg_str, null, null, jwks);
			var claims = client._decode_token(access_token, 
			                              ctx, 
			                              ctx.iau,
			                              ID4meClient.TokenDecodeType.Other,
			                              new TimeSpan(100 * 365, 0, 0, 0),
			                              false);
			
			Assert.IsNotNull(claims, "No claims decoded");
			Assert.AreEqual("john.doe.domainid.community", (string) claims["id4me"]);
			Assert.AreEqual("email_verified;nickname;given_name;email;picture", string.Join(";", claims["clm"].Values()));
			Assert.AreEqual("rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY", (string) claims["sub"]);
			Assert.AreEqual("https://id.test.denic.de", (string) claims["iss"]);
			Assert.AreEqual("john.doe.domainid.community", (string) claims["id4me.identifier"]);
			Assert.AreEqual("2oy-DTcI66I", (string) claims["jti"]);
			Assert.AreEqual("openid", string.Join(";", claims["scope"].Values()));
			Assert.AreEqual("1552314866", (string) claims["exp"]);
			Assert.AreEqual("1552314266", (string) claims["iat"]);
			Assert.AreEqual("3zmce5bcsbxvm", (string) claims["client_id"]);
			Assert.AreEqual("john.doe.domainid.community", (string) claims["identifier"]);


		
			var claims_id = client._decode_token(id_token, 
			                                     ctx, 
			                                     ctx.iau,
												 ID4meClient.TokenDecodeType.IDToken,			                                     
			                                     new TimeSpan(100 * 365, 0, 0, 0));

			Assert.IsNotNull(claims_id, "No claims decoded");
			Assert.AreEqual("rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY", (string) claims_id["sub"]);
			Assert.AreEqual("https://id.test.denic.de", (string) claims_id["iss"]);
			Assert.AreEqual("john.doe.domainid.community", (string) claims_id["id4me.identifier"]);
			Assert.AreEqual("1552314886", (string) claims_id["exp"]);
			Assert.AreEqual("1552313986", (string) claims_id["iat"]);
			Assert.AreEqual("3zmce5bcsbxvm", (string) claims_id["aud"]);
			Assert.AreEqual("pwd", string.Join(";", claims_id["amr"].Values()));

			var claims_agent = client._decode_token(user_info_agent, ctx, 
			                                     "identityagent.de",
			                                     ID4meClient.TokenDecodeType.UserInfo,
			                                     new TimeSpan(100 * 365, 0, 0, 0));
			
			
			Assert.IsNotNull(claims_agent, "No claims decoded");
			Assert.AreEqual("3zmce5bcsbxvm", (string) claims_agent["aud"]);
			Assert.AreEqual("https://identityagent.de", (string) claims_agent["iss"]);
			Assert.AreEqual("1552316290", (string) claims_agent["updated_at"]);
			Assert.AreEqual("mail@test.de", (string) claims_agent["email"]);
			Assert.AreEqual("john.doe.domainid.community", (string) claims_agent["id4me.identifier"]);
			Assert.AreEqual("1552316599", (string) claims_agent["exp"]);
			Assert.AreEqual("john.doe.domainid.community", (string) claims_agent["id4me.identity"]);
			Assert.AreEqual("Nickname", (string) claims_agent["nickname"]);
			Assert.AreEqual("http://picture.com/picture", (string) claims_agent["picture"]);
			Assert.AreEqual("rsLX3XpoDHnGxXt+zFl5VS9c0va5EBdu48opGcaHMR4vaFdpLvq4c+7/ZcAHtCsY", (string) claims_agent["sub"]);
			Assert.AreEqual("1552316299", (string) claims_agent["iat"]);
			Assert.AreEqual("1552316299", (string) claims_agent["nbf"]);			
		}	
	}
	
	
}
