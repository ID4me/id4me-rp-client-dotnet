﻿# ID4me.RP.Client
.NET Relying Party client library for ID4me protocol.
For details of the protocol, please visit: https://id4me.org

Library offers Relying Party functionality for authentication with Identity Authority and claim request from the Identity Agent..

## Specification reference
https://gitlab.com/ID4me/documentation/blob/master/id4ME%20Technical%20Specification.adoc
- Version: 1.0
- Revision: 02

## Installation
### Package Manager
```shell
PM> Install-Package ID4me.RP.Client -Version 0.0.5
```

### .NET CLI
```shell
> dotnet add package ID4me.RP.Client --version 0.0.5
```

### Paket CLI
```shell
> paket add ID4me.RP.Client --version 0.0.5
```


## Usage

### Register the client and authorize with Identity Authority

```c#
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using org.id4me;
using org.id4me.Exceptions;

namespace id4me.net.demo
{
	class Program
	{
		
		// persisted authority list
		const string authstore = "authorities.json";

		// read persisted authorities
		static Dictionary<string, string> getAuthorities()
		{
			if (File.Exists(authstore))
				return JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(authstore));
			else
				return new Dictionary<string, string>();
		}
		
		// read authority from persistence (shall be cached)
		private static string getAuthority(string auth)
		{
			var authorities = getAuthorities();
			return authorities[auth];
		}
		
		// write authority to persistence
		private static void saveAuthority(string auth, string value)
		{
			var authorities = getAuthorities();
			authorities[auth] = value;
			File.WriteAllText("authorities.json", JsonConvert.SerializeObject(authorities));
		}
		
		public static void Main(string[] args)
		{
			// set up the client with needed parameters
			var client = new ID4meClient(
				validate_url: "https://dynamicdns.domainconnect.org/ddnscode",
				client_name: "Demo Client",
				find_authority: getAuthority, 
				save_authority: saveAuthority,
				app_type: OIDCApplicationType.web,
				preferred_client_id: "test_client_id",
				logo_url: "http://localhost:8089/logo.png",
				tos_url: "http://localhost:8089/about",
				policy_url: "http://localhost:8089/documents"
			);
			
			try
			{		
				// get user ID				
				var defaultid = "id200.connect.domains";
				Console.Write(string.Format("ID ({0}): ", defaultid));
				var id4me = Console.ReadLine();
				
				if (string.IsNullOrEmpty(id4me))
					id4me = defaultid;
				
				// get context first (will register client at authority if needed
				var context = client.get_rp_context(id4me);
				Console.WriteLine(context.iau);
				
				// consent URL to get code
				var url = client.get_consent_url(
					context,
					claimsrequest: new ID4meClaimsRequest() {
						userinfo = new Dictionary<string, OIDCClaimRequestOptions> ()
						{
							{ OIDCClaimName.preferred_username, new OIDCClaimRequestOptions(essential: true) },
							{ OIDCClaimName.email, new OIDCClaimRequestOptions(reason: "To keep you informed") }
						}
					}
				);
				// Try to open default browser with the URL
				try
				{
					Console.WriteLine("Opening URL: " + url);
					System.Diagnostics.Process.Start(url);
				}
				catch {
					Console.WriteLine("Press enter to open the URL and paste code here: " + url);
				}
				
				// waiting for code from the user
				Console.Write("Waiting for code: ");					
				var code = Console.ReadLine();
				
				// reading ID token
				var idtoken = client.get_idtoken(context, code);
				
				// Printing out unique ID of an identity
				Console.WriteLine(string.Format("*** ID token (iss/sub): {0} {1}", idtoken["iss"], idtoken["sub"]));
				
				// reading user info
				var user_info = client.get_user_info(context);
				
				// Check if claim preferred_username exists and print out
				if (user_info.ContainsKey(OIDCClaimName.preferred_username))
					Console.WriteLine("*** User Name: " + user_info[OIDCClaimName.preferred_username]);
				// Check if claim email exists and print out
				if (user_info.ContainsKey(OIDCClaimName.email))
					Console.WriteLine("*** E-mail: " + user_info[OIDCClaimName.email]);
			}
			catch (ID4meException e)
			{
				Console.WriteLine("ID4me Exception: " + e.Message);
			}	
			
			Console.Write("Press any key to close...");
			Console.ReadLine();
		}
	}
}
```

### Requesting custom claims

TBD

Example
```C#
				var url = client.get_consent_url(
					context,
					claimsrequest: new ID4meClaimsRequest() {
						userinfo = new Dictionary<string, OIDCClaimRequestOptions> ()
						{
							{ OIDCClaimName.preferred_username, new OIDCClaimRequestOptions(essential: true) },
							{ OIDCClaimName.email, new OIDCClaimRequestOptions(reason: "To keep you informed") },
							{ "id4me.custom", new OIDCClaimRequestOptions(reason: "Spying on you") },
							{ "othercustom", new OIDCClaimRequestOptions(essential: true) }
						}
					}
				);
...
```

## CHANGELOG:
| version | date       | changes |
| ------- | -----------| ------ |
| 0.0.5   | 2019-03-12 | Client registration is renewed automatically when registration expired |
| 0.0.4   | 2019-03-03 | Added DEMO app<br>Updated README with example<br>BUGFIX: token with different id4me was accepted |
| 0.0.3   | 2019-02-28 | Correct dependency to jose-jwt and moved to "private" implementation of jwk until that library supports |
| 0.0.1   | 2019-02-27 | Initial release |
