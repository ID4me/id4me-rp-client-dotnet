﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 01:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using ID4me.Jose.jwk;

namespace org.id4me.Helpers
{	
	public static class JWKExtensionMethods
	{
		private static Dictionary<string, string> jwa_alg_map = new Dictionary<string, string> {
			//signature map
			{"HS256", "oct"},
			{"HS384", "oct"},
			{"HS512", "oct"},
			{"RS256", "RSA"},
			{"RS384", "RSA"},
			{"RS512", "RSA"},
			{"ES256", "ES"},
			{"ES384", "ES"},
			{"ES512", "ES"},
			//encryption map
			{"A128KW", "oct"},
			{"A192KW", "oct"},
			{"A256KW", "oct"},
			{"RSA-OAEP", "RSA"},
			{"RSA-OAEP-256", "RSA"},
			{"ECDH-ES+A128KW", "ES"},
			{"ECDH-ES+A192KW", "ES"},
			{"ECDH-ES+A256KW", "ES"},
		};
		
		public static JWK GetKey(this IEnumerable<JWK> keys, 
		                         string kid, 
		                         string alg, 
		                         bool forSigning,
		                         bool forEncryption)
		{
			JWK ret = null;
			var kty = jwa_alg_map[alg];
			foreach(var key in keys)
			{
				if (key.KeyType.Equals(kty)
				    && (!forEncryption || key.IsEncryption)
				    && (!forSigning || key.IsSignature))
				{
					if (key.Id.Equals(kid))
						return key;
					else
						ret = key;
				}
			}
			return ret;
		}
	}
}




