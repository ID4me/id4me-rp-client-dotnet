﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 01:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace org.id4me.Helpers
{
	/// <summary>
	/// Description of WebRequestHelper.
	/// </summary>
	/// 
	public static class JsonExtensionMethods
	{
		public static bool Contains(this IEnumerable<JToken> list, string value)
		{			
			foreach(var val in list)
				if (((string) val).Equals(value))
					return true;
			return false;
		}
	}
	
	
}


