﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 01:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

namespace org.id4me.Helpers
{
	public class WebRequestException: Exception
	{
		public WebRequestException(string message)
			: base(message)			
		{
			// NOP
		}
	}

	public class WebRequestUnexpectedResponseException: Exception
	{
		public HttpStatusCode StatusCode {
			get; 
			private set;
		}
		
		public WebRequestUnexpectedResponseException(HttpStatusCode statusCode, string message = null)
			: base(message)
		{
			this.StatusCode = statusCode;
		}
		
		public override string ToString()
		{
			return string.Format("[WebRequestUnexpectedResponseException StatusCode={0}, message={1}]", StatusCode, Message);
		}

	}
	
	public static class WebRequestHelper
	{
		static WebRequestHelper()
		{
			ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls12;			
		}
		
		public static JObject GetJson(string url,
		                              IList<string> acceptedContentType = null,
		                              NetworkCredential credential = null,
		                              string BearerToken = null
		                             )
		{				
			var uri = new Uri(url);
			var basePath = url.Substring(0, url.Length - uri.AbsolutePath.Length + 1);
			var resource = uri.AbsolutePath.Substring(1);
			RestClient webClient = new RestClient(basePath);
			RestRequest request = new RestRequest(resource, Method.GET);
			if (credential != null)
				webClient.Authenticator = new HttpBasicAuthenticator(credential.UserName, credential.Password);
			else if (!String.IsNullOrEmpty(BearerToken))
				request.AddHeader("Authorization", string.Format("Bearer {0}", BearerToken));
			if (acceptedContentType != null)
				request.AddHeader("Accept", string.Join(", ", acceptedContentType));
			else
				request.AddHeader("Accept", "application/json");
			
			try 
			{
				var resp = webClient.Execute(request);
				
				if (resp.StatusCode == HttpStatusCode.OK
				    && 
				    (resp.ContentType.StartsWith("application/json")
				     || acceptedContentType != null && acceptedContentType.Contains(resp.ContentType.Split(';')[0]))
				    )				    
				{
					var ret = JsonConvert.DeserializeObject<JObject>(resp.Content);
					return ret;
				}
				else 
					throw new WebRequestUnexpectedResponseException(resp.StatusCode);
			}
			catch (Exception e)
			{
				throw new WebRequestException(string.Format("Unexpected error when processing GetJson request to {0}: {1}", url, e.Message));
			}

		}

		public static string GetRaw(string url,
		                              IList<string> acceptedContentType = null,
		                              NetworkCredential credential = null,
		                              string BearerToken = null
		                             )
		{				
			var uri = new Uri(url);
			var basePath = url.Substring(0, url.Length - uri.AbsolutePath.Length + 1);
			var resource = uri.AbsolutePath.Substring(1);
			RestClient webClient = new RestClient(basePath);
			RestRequest request = new RestRequest(resource, Method.GET);
			if (credential != null)
				webClient.Authenticator = new HttpBasicAuthenticator(credential.UserName, credential.Password);
			else if (!String.IsNullOrEmpty(BearerToken))
				request.AddHeader("Authorization", string.Format("Bearer {0}", BearerToken));
			if (acceptedContentType != null)
				request.AddHeader("Accept", string.Join(", ", acceptedContentType));
			
			try 
			{
				var resp = webClient.Execute(request);
				
				if (resp.StatusCode == HttpStatusCode.OK
				    && 
				    (acceptedContentType != null && acceptedContentType.Contains(resp.ContentType.Split(';')[0]))
				    )				    
				{
					return resp.Content;
				}
				else 
					throw new WebRequestUnexpectedResponseException(resp.StatusCode);
			}
			catch (Exception e)
			{
				throw new WebRequestException(string.Format("Unexpected error when processing GetJson request to {0}: {1}", url, e.Message));
			}

		}		
		
		public static JObject PostJsonWithJsonResponse(
			string url, 
			object o, 
			IList<string> acceptedContentType = null, 
			IList<HttpStatusCode> acceptedStatusCode = null,
			NetworkCredential credential = null,
		    string BearerToken = null)
		{				
			var json = JsonConvert.SerializeObject(o);
			
			var uri = new Uri(url);
			var basePath = url.Substring(0, url.Length - uri.AbsolutePath.Length + 1);
			var resource = uri.AbsolutePath.Substring(1);
			RestClient webClient = new RestClient(basePath);
			RestRequest request = new RestRequest(resource, Method.POST);
			if (credential != null)
				webClient.Authenticator = new HttpBasicAuthenticator(credential.UserName, credential.Password);
			else if (!String.IsNullOrEmpty(BearerToken))
				request.AddHeader("Authorization", string.Format("Bearer {0}", BearerToken));
			if (acceptedContentType != null)
				request.AddHeader("Accept", string.Join(", ", acceptedContentType));
			else
				request.AddHeader("Accept", "application/json");
			request.AddHeader("Content-Type", "application/json");
			request.AddParameter("application/json", json, ParameterType.RequestBody);
			
			try 
			{
				var resp = webClient.Execute(request);
			
				if (
					((acceptedStatusCode == null && resp.StatusCode == HttpStatusCode.OK) 
					|| (acceptedStatusCode != null && acceptedStatusCode.Contains(resp.StatusCode)))
				    && (resp.ContentType.StartsWith("application/json")
				        || acceptedContentType != null && acceptedContentType.Contains(resp.ContentType.Split(';')[0])))
				{
					var ret = JsonConvert.DeserializeObject<JObject>(resp.Content);
					return ret;
				}
				else 
					throw new WebRequestUnexpectedResponseException(resp.StatusCode);
			}
			catch (Exception e)
			{
				throw new WebRequestException(string.Format("Unexpected error when processing PostJsonWithJsonResponse request to {0}: {1}", url, e.Message));
			}

		}

		public static JObject PostFormEncodedWithJsonResponse(
			string url, 
			string formdata, 
			IList<string> acceptedContentType = null, 
			NetworkCredential credential = null,
			string BearerToken = null)
		{							
			var uri = new Uri(url);
			var basePath = url.Substring(0, url.Length - uri.AbsolutePath.Length + 1);
			var resource = uri.AbsolutePath.Substring(1);
			RestClient webClient = new RestClient(basePath);
			RestRequest request = new RestRequest(resource, Method.POST);
			if (credential != null)
				webClient.Authenticator = new HttpBasicAuthenticator(credential.UserName, credential.Password);
			else if (!String.IsNullOrEmpty(BearerToken))
				request.AddHeader("Authorization", string.Format("Bearer {0}", BearerToken));
			if (acceptedContentType != null)
				request.AddHeader("Accept", string.Join(", ", acceptedContentType));
			else
				request.AddHeader("Accept", "application/json");
			request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
			request.AddParameter("application/x-www-form-urlencoded", formdata, ParameterType.RequestBody);
			
			try 
			{
				var resp = webClient.Execute(request);
			
				if (resp.StatusCode == HttpStatusCode.OK
				    && (resp.ContentType.StartsWith("application/json")
				        || acceptedContentType != null && acceptedContentType.Contains(resp.ContentType.Split(';')[0])))
				{
					var ret = JsonConvert.DeserializeObject<JObject>(resp.Content);
					return ret;
				}
				else 
					throw new WebRequestUnexpectedResponseException(resp.StatusCode);
			}
			catch (Exception e)
			{
				throw new WebRequestException(string.Format("Unexpected error when processing PostJsonWithJsonResponse request to {0}: {1}", url, e.Message));
			}
		}				
	}
}
