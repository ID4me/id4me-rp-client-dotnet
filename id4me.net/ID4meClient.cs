﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 25/11/2018
 * Time: 23:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Jose;
using ID4me.Jose.jwk;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using org.id4me;
using org.id4me.Exceptions;
using org.id4me.Helpers;
using Ubiety.Dns.Enums;
using Ubiety.Dns.Query;
using System.Security.Cryptography;
using System.Web;

[assembly: InternalsVisibleTo("id4me.net.tests")]

namespace org.id4me
{
	public class ID4meContext
	{
		public string id { get; internal set; }
		public string iau { get; internal set; }
		public string issuer { get; internal set; }
		public string access_token { get; internal set; }
		public string refresh_token { get; internal set; }
		public string iss { get; internal set; }
		public string sub { get; internal set; }
		public string nonce { get; internal set; }
		
	    public ID4meContext(string id4me, string identity_authority, string issuer)
	    {
			this.id = id4me;
			this.iau = identity_authority;
			this.issuer = issuer;
	        this.access_token = null;
	        this.refresh_token = null;
	        this.iss = null;
	        this.sub = null;
	    }
	}
	
	/// <summary>
	/// Description of ID4meClient.
	/// </summary>
	public sealed class ID4meClient
	{		
		public enum TokenDecodeType {
			IDToken,
			UserInfo,
			Other
		}
		
		private string _validateUrl;
		private string _jwksUrl;
		private string _client_name;
		private string _preferred_client_id;
		private string _logoUrl;
		private string _policyUrl;
		private string _tosUrl;
		private IEnumerable<JWK> _private_jwks;
		private bool? _requireencryption;
		private OIDCApplicationType _app_type;
		private Func<string, string> _get_client_registration_delegate;
		private Action<string, string> _save_client_registration_delegate;
		
	    public ID4meClient(
			string validate_url,
	        string client_name,
	    	Func<string, string> find_authority, 
	    	Action<string, string> save_authority = null,
	        string jwks_url=null,
	        OIDCApplicationType app_type=OIDCApplicationType.native,
	        string preferred_client_id=null,
	        string logo_url=null,
	        string policy_url=null,
	        string tos_url=null,
	        string private_jwks=null,
	        bool? requireencryption=null)
		{
			this._validateUrl = validate_url;
			this._jwksUrl = jwks_url;
			this._client_name = client_name;
			this._preferred_client_id = preferred_client_id;
			this._logoUrl = logo_url;
			this._policyUrl = policy_url;
			this._tosUrl = tos_url;
			this._private_jwks = !String.IsNullOrEmpty(private_jwks) ? JWKS.Parse(private_jwks) : null;
			this._requireencryption = requireencryption;
			this._app_type = app_type;
			this._get_client_registration_delegate = find_authority;
			this._save_client_registration_delegate = save_authority;
			if (requireencryption.HasValue && requireencryption.Value == true && private_jwks == null)
				throw new ID4meAuthorityConfigurationException("Encryption requires private keys");
		}
		
		private JObject _get_client_registration(string auth)
		{
			return JsonConvert.DeserializeObject<JObject>(_get_client_registration_delegate(auth));
		}
		
	    internal static string _get_identity_authority(string id4me)
	    {
	    	var hostparts = id4me.Split('.');
	    	Exception firstExc = null;
	    	
	    	for (var i = 0; i < hostparts.Length; i++)
	    	{
	    		var hostname = string.Join(".", hostparts.Skip(i));
	    		try
	    		{
	    			return _get_identity_authority_single(hostname);
	    		}
	    		catch (Exception e)
	    		{
	    			if (firstExc == null)
	    				firstExc = e;
	    		}
	    	}
	    	if (firstExc != null)
	    		throw firstExc;
	    	else
	    		throw new ID4meDNSResolverException(String.Format("No valid _openid record found on any level"));
	    	
	    }


		internal static string _get_identity_authority_single(string id4me)
	    {
	    	var hostname = String.Format("_openid.{0}.", id4me);
	    	#if DEBUG
	    	Console.WriteLine(String.Format("Resolving {0}", hostname));
			#endif
	    	
	        // TODO: enforce strict DNSSEC policy when support added by all parties...
	        // TODO: add configuration to use custom resolver
	        var request = new DnsQueryRequest();
	        try
	        {
				var response = request.Resolve(hostname, QueryType.TXT, QueryClass.IN, ProtocolType.Tcp);
				if (response != null && response.Answers != null && response.Answers.Count > 0)
				{
					foreach (var r in response.Answers)
					{
						#if DEBUG
						Console.WriteLine(r.RecordHeader.QueryType + ", " + r.Answer);
						#endif
						if (r.RecordHeader.QueryType == QueryType.TXT
						    && r.Answer.Substring(1).StartsWith("v=OID1;"))
						{
							foreach (var recpart in r.Answer.Split(';'))
							{
								if (recpart.StartsWith("iau=") || recpart.StartsWith("iss="))
								{
									return recpart.Substring(4);
								}
							}
						}
					}
				}
	        }
	        catch (Exception e)
	        {
	        	throw new ID4meDNSResolverException(String.Format("Error resolving _openid record: {}", e.Message));
	        }
	        throw new ID4meDNSResolverException(String.Format("No valid _openid record found"));
	    }

	    internal JObject _get_openid_configuration(string issuer)
	    {
	    	try
	    	{
	            var url = String.Format(
	    			"{0}{1}{2}.well-known/openid-configuration",
	    			issuer.StartsWith("https://") ? "" : "https://",
					issuer,
					issuer.EndsWith("/") ? "" : "/"
				);
	    		
	    		return Helpers.WebRequestHelper.GetJson(url);
	    	}
	    	catch (Exception e)
	    	{
	    		throw new ID4meAuthorityConfigurationException(String.Format("Could not get configuration for {0}, {1}", issuer, e.Message));
	    	}            
	    }

	    public static List<JWK> Generate_new_private_keys_set()
	    {
	    	var rsakeypair = RSA.Create();
	    	var jwk = new JWK();
	    	jwk.Key = rsakeypair;
	    	jwk.Id = Guid.NewGuid().ToString();
	    	var jwks = new List<JWK>()
	    	{
	    		jwk
	    	};
	    	return jwks;
	    }

       	private static List<HttpStatusCode> _register_identity_authority_acceptedCodes = new List<HttpStatusCode>() {HttpStatusCode.OK, HttpStatusCode.Created};
	    
	    internal JObject _register_identity_authority(string identity_authority)
	    {
	    	var identity_authority_config = _get_openid_configuration(identity_authority);
	    	#if DEBUG
	    	Console.WriteLine(String.Format("registering with new identity authority ({0})", identity_authority));
			#endif	        	        
	        if (!identity_authority_config["id_token_signing_alg_values_supported"].Children().Contains("RS256"))
	        	throw new ID4meRelyingPartyRegistrationException(
	        		"Required signature algorithm for id_token RS256 not supported by Authority");
	        if (!identity_authority_config["userinfo_signing_alg_values_supported"].Children().Contains("RS256"))
	        	throw new ID4meRelyingPartyRegistrationException(
	        		"Required signature algorithm for userinfo RS256 not supported by Authority");
	        
	        var request = new Dictionary<string, object>() {
	        	{ "redirect_uris", new List<string>() {_validateUrl} },
	        	{"id_token_signed_response_alg", "RS256"},
	        	{"userinfo_signed_response_alg", "RS256"}
	        };
			
	        if ((!_requireencryption.HasValue || _requireencryption.Value) && _private_jwks != null)
	        {
		        if (string.IsNullOrEmpty(_jwksUrl))
		        {
			        request["jwks"] = JsonConvert.DeserializeObject(JWKS.Serialize(_private_jwks, false));
		        }
		        else
		        	request["jwks_uri"] = _jwksUrl;

		        if (identity_authority_config["id_token_encryption_alg_values_supported"].Children().Contains("RSA-OAEP-256"))
		        	request["id_token_encrypted_response_alg"] = "RSA-OAEP-256";
		        else if (_requireencryption.HasValue && _requireencryption.Value)
		        	throw new ID4meRelyingPartyRegistrationException(
		        		"Required encryption algorithm for id_token RSA-OAEP-256 not supported by Authority");
		        if (identity_authority_config["userinfo_encryption_alg_values_supported"].Children().Contains("RSA-OAEP-256"))
		        	request["userinfo_encrypted_response_alg"] = "RSA-OAEP-256";
		        else if (_requireencryption.HasValue && _requireencryption.Value)
		        	throw new ID4meRelyingPartyRegistrationException(
		        		"Required encryption algorithm for userinfo RSA-OAEP-256 not supported by Authority");
	        }
	
	        if (!String.IsNullOrEmpty(_preferred_client_id))
	        	request["preferred_client_id"] = _preferred_client_id;
	        if (!String.IsNullOrEmpty(_client_name))
	        	request["client_name"] = _client_name;
	        if (!String.IsNullOrEmpty(_logoUrl))
	        	request["logo_uri"] = _logoUrl;	        
	        if (!String.IsNullOrEmpty(_policyUrl))
	        	request["policy_uri"] = _policyUrl;
       		if (!String.IsNullOrEmpty(_tosUrl))
       			request["tos_uri"] = _tosUrl;
	
       		request["application_type"] = _app_type.ToString();
	        
       		try
       		{
	       		var registration = WebRequestHelper.PostJsonWithJsonResponse(
	       			(string) identity_authority_config["registration_endpoint"],
	       			request, null, _register_identity_authority_acceptedCodes);
       			if (registration.ContainsKey("error") && registration.ContainsKey("error_description"))
       				throw new ID4meRelyingPartyRegistrationException(string.Format("Error registering Relying Party at {0}: {1}", 
       				                                                               identity_authority, 
       				                                                               (string) registration["error_description"]));
       			if (!registration.ContainsKey("client_id") && registration.ContainsKey("client_secret"))
       				throw new ID4meRelyingPartyRegistrationException(
       					"client_id or client_secret not returned by the Authority during registration");
       			return registration;
       		}
       		catch (WebRequestException e)
       		{
       			throw new ID4meRelyingPartyRegistrationException(string.Format("Could not register {0}: {1}", identity_authority, e.Message));
       		}
	    }

		/*
		 * Makes discovery of ID4me identifier id4me. Performs registration at relevant authority,
         * if necessary or recalls a saved authority data via a callback
         * :param str id4me: ID4me identifier
         * :param fun(str)->str find_authority: callback to lookup authority registration - params - name, ret - value
         * :param fun(str, str)->None save_authority: callback to save authority settings - params - name, value
         * :return: context of ID to use with other flows
         * :rtype: ID4meContext
         * :raises ID4meRelyingPartyRegistrationException: in case of issues with registration
		 */
	    public ID4meContext get_rp_context(string id4me) 
		{
			var identity_authority = _get_identity_authority(id4me);
			var auth_configuration = _get_openid_configuration(identity_authority);
			#if DEBUG
			Console.WriteLine(string.Format("identity_authority = {0}", identity_authority));
			#endif
			
			JObject registration = null;
            try {
				registration = _get_client_registration(identity_authority);
				if (registration.ContainsKey("client_secret_expires_at")
					&& registration.Value<int>("client_secret_expires_at") != 0)
				{	
					var expire = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
						.AddSeconds(registration.Value<int>("client_secret_expires_at"));						
					if (expire.Subtract(DateTime.UtcNow) < new TimeSpan(2, 0, 0))
						registration = null;
				}
			}
            // disable once EmptyGeneralCatchClause
            catch 
            {
            	// ignore all exceptions (we'll try to re-register as fallback)
            }
		
			if (registration == null) {
				registration = _register_identity_authority(identity_authority);
	        	if (_save_client_registration_delegate != null)
	        	{
	        		_save_client_registration_delegate(identity_authority, JsonConvert.SerializeObject(registration));
	        	}
			}
			var context = new ID4meContext(id4me, identity_authority, (string) auth_configuration["issuer"]);
			return context;
		}

		private IEnumerable<JWK> _get_public_keys_set(string jwks_uri)
		{
			IEnumerable<JWK> ret;
	        try
	        {
	        	//HACK: "text/html" added because of MojeID strange content type
	        	var jwks = WebRequestHelper.GetJson(jwks_uri, new List<string>{"application/json", "application/jwk-set+json", "text/html"});
	        	ret = JWKS.Parse(JsonConvert.SerializeObject(jwks));
	        }
	        catch (Exception ex)
	        {
	        	throw new ID4meAuthorityConfigurationException(string.Format("Could not get public keys for {0}, {1}", jwks_uri, ex));
	        }
	        return ret;
		}
				
		internal JObject _decode_token(
			string token, 
			ID4meContext context, 
			string iss,
			TokenDecodeType tokentype,
			TimeSpan? leeway=null, 
			bool verify_aud=true,
			Func<string, IEnumerable<JWK>> getAuthJWKS = null)
		{		
			try
			{				
				if (leeway == null)
					leeway = new TimeSpan(0, 5, 0);
				var decodedTokenHeaders = Jose.JWT.Headers(token);
							
				if (decodedTokenHeaders.ContainsKey("enc"))
				{
					if (_private_jwks == null)
						throw new ID4meTokenException("Cannot decode encrypted token. Priate JWKS not set");
					var encKey = _private_jwks.GetKey(
						decodedTokenHeaders["kid"].ToString(),
						decodedTokenHeaders["alg"].ToString(),
						false, true);
					return _decode_token(
						JWT.Decode(token, encKey.Key),
						context,
						iss,
						tokentype,
						leeway,
						verify_aud,
						getAuthJWKS);
				}
				else
				{				
					var registration = _get_client_registration(context.iau);
					
					if (_requireencryption.HasValue
					    && _requireencryption.Value)
						throw new ID4meTokenException("Token not encrypted when expected");
					
					if (decodedTokenHeaders.ContainsKey("typ") && !decodedTokenHeaders["typ"].Equals("JWT"))
						throw new ID4meTokenException("Invalid or missing token type");
	
					if (!decodedTokenHeaders.ContainsKey("alg"))
						throw new ID4meTokenException("Invalid or missing token signature algorithm");					
					
					if (tokentype == TokenDecodeType.IDToken
					    && registration.ContainsKey("id_token_signed_response_alg") 
					    && !((string)decodedTokenHeaders["alg"]).Equals((string) registration["id_token_signed_response_alg"]))
						throw new ID4meTokenException(string.Format("Invalid token signature algorithm. Expected: {0}, Received: {1}", 
						                                        (string) registration["id_token_signed_response_alg"], decodedTokenHeaders["alg"]));
					if (tokentype == TokenDecodeType.UserInfo
					    && registration.ContainsKey("userinfo_signed_response_alg") 
					    && !((string)decodedTokenHeaders["alg"]).Equals((string) registration["userinfo_signed_response_alg"]))
						throw new ID4meTokenException(string.Format("Invalid token signature algorithm. Expected: {0}, Received: {1}", 
						                                        (string) registration["userinfo_signed_response_alg"], decodedTokenHeaders["alg"]));					
					IEnumerable<JWK> keys;
					if (getAuthJWKS != null)
					{
						keys = getAuthJWKS(iss);
					}
					else
					{
						var issuer_config = _get_openid_configuration(iss);
						keys = _get_public_keys_set((string) issuer_config["jwks_uri"]);						
					}
					
					var key = keys.GetKey(decodedTokenHeaders.ContainsKey("kid") ? decodedTokenHeaders["kid"].ToString() : null,
					                      decodedTokenHeaders["alg"].ToString(),
					                      true, false);
					
					var decodedToken = JWT.Decode(token, key.Key);
					
					var tokenObj = JsonConvert.DeserializeObject<JObject>(decodedToken);
					
					if (verify_aud && tokenObj.ContainsKey("aud")
					    && !(
					    	tokenObj["aud"] is JArray && (tokenObj["aud"] as JArray).ToObject<string[]>().Contains((string) registration["client_id"])
					         || 
					         !(tokenObj["aud"] is JArray) && ((string)tokenObj["aud"]).Equals((string) registration["client_id"])))
						throw new ID4meTokenException(String.Format("Token with wrong audience. Expected {0}, is {1}", (string) registration["client_id"], (string)tokenObj["aud"]));
	
					if (verify_aud && tokenObj.ContainsKey("azp") && !((string)tokenObj["azp"]).Equals((string) registration["client_id"]))
						throw new ID4meTokenException(String.Format("Token with wrong azp. Expected {0}, is {1}", (string) registration["client_id"], (string)tokenObj["azp"]));				
	
					if (!String.IsNullOrEmpty(context.sub) 
					    && tokenObj.ContainsKey("sub")
					    && !((string)tokenObj["sub"]).Equals(context.sub))
						throw new ID4meTokenException(String.Format("Token with wrong sub. Expected {0}, is {1}", context.sub, (string)tokenObj["sub"]));				
					
					// ID token specific verifi cation rules
					if (tokentype == TokenDecodeType.IDToken)
					{
						if (!tokenObj.ContainsKey("iss"))
						    throw new ID4meTokenException("Issuer missing in ID Token");
						if (!((string)tokenObj["iss"]).Equals(context.issuer))
					    	throw new ID4meTokenException(string.Format("Wrong issuer for ID Token. Expected: {0}, Received: {1}", context.iau, tokenObj["iss"]));
						if (tokenObj["aud"] is JArray && (tokenObj["aud"] as JArray).Count > 1 && !tokenObj.ContainsKey("azp"))
					    	throw new ID4meTokenException("Multiple aud in token, but missing azp");
						if (!string.IsNullOrEmpty(context.nonce))
						{
							if (!tokenObj.ContainsKey("nonce"))
								throw new ID4meTokenException("Nonce missing and expected from the context");
							if (!((string) tokenObj["nonce"]).Equals(context.nonce))
								throw new ID4meTokenException(string.Format("Wrong nonce. Expected: {0}, Received: {1}", context.nonce, tokenObj["nonce"]));
						}
						if (tokenObj.ContainsKey("nonce") && string.IsNullOrEmpty(context.nonce))
							throw new ID4meTokenException(string.Format("Nonce replay detected. Expected: None, Received: {0}", tokenObj["nonce"]));
						if (tokenObj.ContainsKey("id4me.identifier") && (string) tokenObj["id4me.identifier"] != context.id)
							throw new ID4meTokenException(string.Format("ID4me mismatch in token. Expected: {0}, Received: {1}", context.id, tokenObj["id4me.identifier"]));
					}
					
					if (tokenObj.ContainsKey("exp"))
					{
						var expDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) + new TimeSpan(0, 0, (int)tokenObj["exp"]);
						if (DateTime.UtcNow > expDate + leeway)
							throw new ID4meTokenException(String.Format("Token expired on {0}", expDate));
					}
					
					// validation finished
					// if there is nonce in the context and ID_token was decoded - remove nonce
					if (tokentype == TokenDecodeType.IDToken)
					{
						context.nonce = null;
					}
					return tokenObj;
				}
			}
			catch (JoseException je)
			{
				throw new ID4meTokenException(string.Format("Token exception: {0}", je.Message));
			}
		}

		private readonly JsonSerializerSettings claimsSerializerSettings = new JsonSerializerSettings()
		{
			NullValueHandling = NullValueHandling.Ignore
		};
		
		//TODO: document input parameters
		public string get_consent_url(ID4meContext context, 
		                              string state="", 
		                              ID4meClaimsRequest claimsrequest=null,
		                              string prompt=null,
		                              bool useNonce=false)
		{
			
			var auth_config = _get_openid_configuration(context.iau);
			var registration = _get_client_registration(context.iau);
			var endpoint = string.Format("{0}", _validateUrl);
			
			var destination = string.Format(
				"{0}?scope=openid&response_type=code&client_id={1}&redirect_uri={2}&login_hint={3}&state={4}",
				(string) auth_config["authorization_endpoint"],
				HttpUtility.UrlEncode((string) registration["client_id"]),
				HttpUtility.UrlEncode(endpoint),
				HttpUtility.UrlEncode(context.id),
				HttpUtility.UrlEncode(state));

			if (prompt != null)
				destination = string.Format(
					"{0}&prompt={1}",
					destination,
					HttpUtility.UrlEncode(prompt));
			
			if (useNonce)
			{
				context.nonce = Guid.NewGuid().ToString();
				destination = string.Format(
					"{0}&nonce={1}",
					destination,
					HttpUtility.UrlEncode(context.nonce));				
			}
			else
				context.nonce = null;
			
			if (claimsrequest != null)
			{
				var claims = JsonConvert.SerializeObject(claimsrequest, claimsSerializerSettings);
				destination = string.Format(
					"{0}&claims={1}",
					destination,
					HttpUtility.UrlEncode(claims));				
			}
			
			#if DEBUG
			Console.WriteLine(string.Format("destination = {0}", destination));
			#endif
			
			return destination;
		}
		
		public JObject get_idtoken(ID4meContext context, string code)
		{
			//TODO: document input parameters
			//TODO: add OAuth error handling (offload client)
			
			var auth_config = _get_openid_configuration(context.iau);
			var registration = _get_client_registration(context.iau);
			
			var data = string.Format(
				"grant_type=authorization_code&code={0}&redirect_uri={1}",
				HttpUtility.UrlEncode(code), HttpUtility.UrlEncode(_validateUrl));
			JObject resp = null;
			try
			{
				resp = WebRequestHelper.PostFormEncodedWithJsonResponse(
					(string) auth_config["token_endpoint"],
					data, null, new NetworkCredential((string) registration["client_id"], (string) registration["client_secret"]));
					
			}
			catch (Exception e)
			{
				throw new ID4meTokenRequestException(
					string.Format("Failed to get id token from {0} ({1})",
					              context.iau, e.Message));
			}
			
			if (resp.ContainsKey("access_token") 
			    && resp.ContainsKey("token_type")
			    && ((string) resp["token_type"]).Equals("Bearer"))
				context.access_token = (string) resp["access_token"];
            else
            	throw new ID4meTokenRequestException("Access token missing in authority token response");
			if (resp.ContainsKey("refresh_token"))
				context.refresh_token = (string) resp["refresh_token"];
			if (resp.ContainsKey("id_token"))
			{
				var payload = _decode_token((string) resp["id_token"], context, context.iau, TokenDecodeType.IDToken);
				context.iss = (string) payload["iss"];
				context.sub = (string) payload["sub"];
				return payload;
			}
			else
				throw new ID4meTokenRequestException("ID token missing in authority token response");
		}		

		public JObject get_user_info(ID4meContext context)
			//TODO: document input parameters
		{
			if (context.access_token == null)
				throw new ID4meUserInfoRequestException("No access token is session. Call get_id_token() first.");
            //TODO: we need to check access token for expiry and renew with refresh_token if expired (and avail.)
            var auth_config = _get_openid_configuration(context.iau);
            string user_info_jwt = null;
			try
			{
				user_info_jwt = WebRequestHelper.GetRaw(
					(string) auth_config["userinfo_endpoint"], 
					new List<string> {"application/jwt"}, 
					null, 
					context.access_token);
			}
			catch (Exception e)
			{
				throw new ID4meTokenRequestException(
					string.Format(
						"Failed to get user info from {0} ({1})",
						(string) auth_config["userinfo_endpoint"], e.Message)
				);
			}
			var user_info = new JObject();
			user_info["iss"] = context.iss;
			user_info["sub"] = context.sub;
			_decode_user_info(context, user_info_jwt, user_info, context.iss);
			return user_info;
		}

		private void _decode_user_info(ID4meContext context, 
		                               string jwt, 
		                               JObject user_claims, 
		                               string iss, 
		                               TimeSpan? leeway = null)
		{
			if (leeway == null)
				leeway = new TimeSpan(0, 5, 0);
			var resp = _decode_token(jwt, context, iss, TokenDecodeType.UserInfo, leeway);

			var queried_endpoints = new List<string>();
			if (resp.ContainsKey("_claim_sources") && resp.ContainsKey("_claim_names"))
			{
				foreach (var claim in resp["_claim_names"].Children())
				{
					var clname = (string) claim;
					var cls = resp["_claim_sources"] as JObject;
					if (cls != null)
					{
						if (cls.ContainsKey(clname)
						    && (cls[clname] is JObject) && (cls[clname] as JObject).ContainsKey("access_token")
						    && (cls[clname] is JObject) && (cls[clname] as JObject).ContainsKey("endpoint"))
					    {
							var endpoint = (string) resp["_claim_sources"][clname]["endpoint"];
							var access_token = (string) resp["_claim_sources"][clname]["access_token"];
							if (!queried_endpoints.Contains(endpoint))
							{
								_get_distributed_claims(context, endpoint, access_token, user_claims, leeway.Value);
								queried_endpoints.Add(endpoint);
							}
					    }
					}
				}
			}
				                           
			
			foreach (var key in resp)
			{
				if (key.Key != "_claim_sources" 
				    && key.Key != "_claim_names" 
				    && !user_claims.ContainsKey(key.Key))
					user_claims[key.Key] = key.Value;
			}
		}

		private void _get_distributed_claims(
			ID4meContext context, 
			string endpoint, 
			string access_code,
			JObject user_claims,
			TimeSpan leeway)
		{
			try {
				var resp = WebRequestHelper.GetRaw(
					endpoint, 
					//HACK: "application/json" is not correct here
					new List<string> {"application/jwt", "application/json"}, 
					null, 
					access_code);
				var url = new Uri(endpoint);
				var iss = string.Format("{0}://{1}/", url.Scheme, url.Host);
				_decode_user_info(context, resp, user_claims, iss, leeway);
			}
			catch (Exception e)
			{
				throw new ID4meTokenRequestException(
					string.Format(
						"Failed to get distributed user info from {0} ({1})",
						endpoint, e.Message));
			}
		}

	}
}