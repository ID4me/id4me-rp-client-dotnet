﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 00:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
namespace org.id4me.Exceptions
{
	/// <summary>
	/// Description of ID4meExceptions.
	/// </summary>
	public abstract class ID4meException : Exception
	{
		protected ID4meException(string str)
			: base(str)
		{
			//NOP
		}		
	}
	
	

}


