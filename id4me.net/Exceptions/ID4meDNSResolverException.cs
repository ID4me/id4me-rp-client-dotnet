﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 26/11/2018
 * Time: 00:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace org.id4me.Exceptions
{
	public class ID4meDNSResolverException: ID4meException
	{
		internal ID4meDNSResolverException(string str)
			: base(str)
		{
			//NOP
		}		
	}

	
	
}
