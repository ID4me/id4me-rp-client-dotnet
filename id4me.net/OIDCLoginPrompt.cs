﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 25/11/2018
 * Time: 23:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
namespace org.id4me
{
	public static class OIDCLoginPrompt
	{
		public const string sub = "sub";

		public const string none = "none";

		public const string login = "login";

		public const string consent = "consent";

		public const string login_and_consent = "login consent";

		public const string selectaccount = "select_account";

		public const string selectaccount_and_login = "select_account login";

		public const string selectaccount_and_consent = "select_account consent";

		public const string selectaccount_and_login_and_consent = "select_account login consent";
	}
}



