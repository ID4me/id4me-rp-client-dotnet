﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 25/11/2018
 * Time: 23:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace org.id4me
{
	
	public class ID4meClaimsRequest
	{
		public IDictionary<string, OIDCClaimRequestOptions> id_token {get; set;}
		public IDictionary<string, OIDCClaimRequestOptions> userinfo {get; set;}
	}
	
	public class OIDCClaimRequestOptions
	{
		public OIDCClaimRequestOptions(string reason = null, bool? essential = null)
		{
			this.reason = reason;
			this.essential = essential;
		}
		
		public string reason {get; set; }
		public bool? essential {get; set; }
	}

	public static class OIDCIDTokenClaimName
	{
		public const string auth_time = "auth_time";
		public const string acr = "acr";
	}

	public static class OIDCClaimName
	{
		public const string sub = "sub";

		public const string name = "name";

		public const string given_name = "given_name";

		public const string family_name = "family_name";

		public const string middle_name = "middle_name";

		public const string nickname = "nickname";

		public const string preferred_username = "preferred_username";

		public const string profile = "profile";

		public const string picture = "picture";

		public const string website = "website";

		public const string email = "email";

		public const string email_verified = "email_verified";

		public const string gender = "gender";

		public const string birthdate = "birthdate";

		public const string zoneinfo = "zoneinfo";

		public const string locale = "locale";

		public const string phone_number = "phone_number";

		public const string phone_number_verified = "phone_number_verified";

		public const string address = "address";

		public const string updated_at = "updated_at";
	}
}

