﻿using System.Collections.Generic;
using ID4me.Jose.jwk.util;
using System.Linq;
using Jose;


namespace ID4me.Jose.jwk
{
	
	
	
    public class JWK
    {
        public static JWK Parse(string json, JwtSettings settings = null)
        {
            settings = settings ?? JWT.DefaultSettings;
            IDictionary<string, object> header = settings.JsonMapper
                .Parse<Dictionary<string, object>>(json);
            var ret = settings
                .JwkAlgorithmFromHeader(header.GetString("kty"))
                .Parse(header, settings);
            ret.Header = (from item in ret.Header
            	          where (item.Key == "kty" || item.Key == "use" || item.Key == "kid") select item)
            		.ToDictionary(x => x.Key, x => x.Value);
            return ret;
        }
        public JWK()
        {
            Header = new Dictionary<string,object>();
        }
        public IDictionary<string,object> Header { get; internal set; }
        public string KeyType { 
        	get {
        		return Header.GetString("kty");
        	}
        }
        public string Use { 
        	get {
        		return Header.GetString("use"); 
        	}
        }
        public bool IsSignature { 
        	get {
        		return string.Equals("sig", Use ?? "sig"); 
        	}
        	set {
        		if (value)
	        		Header["use"] = "sig";
        	}
        }
    	public bool IsEncryption { 
        	get {
    			return string.Equals("enc", Use ?? "enc"); 
        	}
        	set {
        		if (value)
        			Header["use"] = "enc";
        	}
        }
        public string Id { 
        	get {
        		return Header.GetString("kid"); 
        	}
        	set {
        		Header["kid"] = value;
        	}
        }
        public object Key { get; set; }
        public string Serialize(bool includePrivateParameters = false, JwtSettings settings = null)
        {
            settings = settings ?? JWT.DefaultSettings;
            IDictionary<string,object> header = settings
                .JwkAlgorithmFromKey(Key)
                .Serialize(this, includePrivateParameters, settings);
            return settings.JsonMapper.Serialize(header);
        }
    }
}
