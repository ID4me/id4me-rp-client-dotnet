﻿using System.Linq;
using System.Security.Cryptography;
using Jose;

namespace ID4me.Jose.jwk
{
	public static class JwtSettingJWKExtension
	{
		//JWK
		public static IJwkAlgorithm JwkAlgorithmFromHeader(this JwtSettings settings, string kty)
		{
			switch (kty) {
				case "oct":
					return new JwkOct();
				case "RSA":
					return new JwkRsa();
//				case "EC":
//					return new JwkEc();
				default:
					throw new InvalidAlgorithmException(string.Format("JWK type not supported: {0}", kty));
			}
		}

		public static IJwkAlgorithm JwkAlgorithmFromKey(this JwtSettings settings, object key)
		{
			if (key is byte[]) {
				return new JwkOct();
			}
			if (key is RSA) {
				return new JwkRsa();
			}
//			if (key is ECDsa) {
//				return new JwkEc();
//			}
			throw new InvalidAlgorithmException(string.Format("Key type not supported: {0}", key.GetType().Name));
		}
	}
}


