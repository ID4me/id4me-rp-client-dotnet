﻿/*
 * Created by SharpDevelop.
 * User: pkowalik
 * Date: 02/03/2019
 * Time: 20:13
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using org.id4me;
using org.id4me.Exceptions;

namespace id4me.net.demo
{
	class Program
	{
		
		// persisted authority list
		const string authstore = "authorities.json";

		// read persisted authorities
		static Dictionary<string, string> getAuthorities()
		{
			if (File.Exists(authstore))
				return JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(authstore));
			else
				return new Dictionary<string, string>();
		}
		
		// read authority from persistence (shall be cached)
		private static string getAuthority(string auth)
		{
			var authorities = getAuthorities();
			return authorities[auth];
		}
		
		// write authority to persistence
		private static void saveAuthority(string auth, string value)
		{
			var authorities = getAuthorities();
			authorities[auth] = value;
			File.WriteAllText("authorities.json", JsonConvert.SerializeObject(authorities));
		}
		
		public static void Main(string[] args)
		{
			// set up the client with needed parameters
			var client = new ID4meClient(
				validate_url: "https://dynamicdns.domainconnect.org/ddnscode",
				client_name: "Demo Client",
				find_authority: getAuthority, 
				save_authority: saveAuthority,
				app_type: OIDCApplicationType.web,
				preferred_client_id: "test_client_id",
				logo_url: "http://localhost:8089/logo.png",
				tos_url: "http://localhost:8089/about",
				policy_url: "http://localhost:8089/documents"
			);
			
			try
			{		
				// get user ID				
				var defaultid = "id200.connect.domains";
				Console.Write(string.Format("ID ({0}): ", defaultid));
				var id4me = Console.ReadLine();
				
				if (string.IsNullOrEmpty(id4me))
					id4me = defaultid;
				
				// get context first (will register client at authority if needed
				var context = client.get_rp_context(id4me);
				Console.WriteLine(context.iau);
				
				// consent URL to get code
				var url = client.get_consent_url(
					context,
					claimsrequest: new ID4meClaimsRequest() {
						userinfo = new Dictionary<string, OIDCClaimRequestOptions> ()
						{
							{ OIDCClaimName.preferred_username, new OIDCClaimRequestOptions(essential: true) },
							{ OIDCClaimName.email, new OIDCClaimRequestOptions(reason: "To keep you informed") }
						}
					}
				);
				// Try to open default browser with the URL
				try
				{
					Console.WriteLine("Opening URL: " + url);
					System.Diagnostics.Process.Start(url);
				}
				catch {
					Console.WriteLine("Press enter to open the URL and paste code here: " + url);
				}
				
				// waiting for code from the user
				Console.Write("Waiting for code: ");					
				var code = Console.ReadLine();
				
				// reading ID token
				var idtoken = client.get_idtoken(context, code);

				// Printing out unique ID of an identity
				Console.WriteLine(string.Format("*** iss sub: {0} {1}", idtoken["iss"], idtoken["sub"]));
				Console.WriteLine(string.Format("*** ID Token: {0}", idtoken));

				// reading user info
				var user_info = client.get_user_info(context);
				
				// Check if claim preferred_username exists and print out
				if (user_info.ContainsKey(OIDCClaimName.preferred_username))
					Console.WriteLine("*** User Name: " + user_info[OIDCClaimName.preferred_username]);
				// Check if claim email exists and print out
				if (user_info.ContainsKey(OIDCClaimName.email))
					Console.WriteLine("*** E-mail: " + user_info[OIDCClaimName.email]);
			}
			catch (ID4meException e)
			{
				Console.WriteLine("ID4me Exception: " + e.Message);
			}	
			
			Console.Write("Press any key to close...");
			Console.ReadLine();
		}
	}
}